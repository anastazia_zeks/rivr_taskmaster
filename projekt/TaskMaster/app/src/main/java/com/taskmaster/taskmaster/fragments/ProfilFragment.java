package com.taskmaster.taskmaster.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import com.taskmaster.taskmaster.PregledNalog;
import com.taskmaster.taskmaster.ProfilIkonaAdapter;
import com.taskmaster.taskmaster.R;
import com.taskmaster.taskmaster.dodatki.DBHelper;
import com.taskmaster.taskmaster.razredi.Level;
import com.taskmaster.taskmaster.razredi.Uporabnik;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfilFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfilFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private DBHelper dbh;
    private Uporabnik uporabnik;

    EditText editText_ime;
    EditText editText_priimek;
    EditText editText_vzdevek;
    Button saveChanges;
    ImageView profileIcon;
    GridView gridView_1level;
    GridView gridView_2level;
    GridView gridView_3level;
    GridView gridView_4level;
    GridView gridView_5level;
    int slika_ikona_id;

    private boolean na_voljo;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfilFragment newInstance(String param1, String param2) {
        ProfilFragment fragment = new ProfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_profil, container, false);

        dbh = new DBHelper(getActivity().getApplicationContext());
        uporabnik=dbh.getLastUporabnik();


        double trenutne_tocke=dbh.getTockeForTocke(uporabnik.getId(),"trenutne");
        String levelByTocke=dbh.getLevelIDByTocke(trenutne_tocke);
        Level levelObject=dbh.getLevel(levelByTocke);


        editText_ime = (EditText) view.findViewById(R.id.ime_id);
        editText_ime.setText(uporabnik.getIme());
        editText_priimek = (EditText) view.findViewById(R.id.priimek_id);
        editText_priimek.setText(uporabnik.getPriimek());
        editText_vzdevek= (EditText) view.findViewById(R.id.vzdevek_id);
        editText_vzdevek.setText(uporabnik.getVzdevek());

        saveChanges = (Button) view.findViewById(R.id.profileSaveButton);
        gridView_1level = (GridView) view.findViewById(R.id.profileIcon_1level);
        gridView_2level = (GridView) view.findViewById(R.id.profileIcon_2level);
        gridView_3level = (GridView) view.findViewById(R.id.profileIcon_3level);
        gridView_4level = (GridView) view.findViewById(R.id.profileIcon_4level);
        gridView_5level = (GridView) view.findViewById(R.id.profileIcon_5level);
        profileIcon = (ImageView) view.findViewById(R.id.slika_ikona_id) ;
        profileIcon.setImageResource(uporabnik.getIkona());
        slika_ikona_id=uporabnik.getIkona();


        ProfilIkonaAdapter profilIkonaAdapter=new ProfilIkonaAdapter(getContext());
        profilIkonaAdapter.setProfileIcons(profileIcons_1level());
        profilIkonaAdapter.setNa_voljo(true);
        profilIkonaAdapter.setLevel(1);
        profilIkonaAdapter.setLevel_uporabnika(levelObject.getStLevela());
        setAdapterGridView(gridView_1level,profilIkonaAdapter);

        ProfilIkonaAdapter profilIkonaAdapter2=new ProfilIkonaAdapter(getContext());
        profilIkonaAdapter2.setProfileIcons(profileIcons_2level());
        profilIkonaAdapter2.setNa_voljo(false);
        profilIkonaAdapter2.setLevel(2);
        profilIkonaAdapter2.setLevel_uporabnika(levelObject.getStLevela());
        setAdapterGridView(gridView_2level,profilIkonaAdapter2);


        ProfilIkonaAdapter profilIkonaAdapter3=new ProfilIkonaAdapter(getContext());
        profilIkonaAdapter3.setProfileIcons(profileIcons_3level());
        profilIkonaAdapter3.setNa_voljo(false);
        profilIkonaAdapter3.setLevel(3);
        profilIkonaAdapter3.setLevel_uporabnika(levelObject.getStLevela());
        setAdapterGridView(gridView_3level,profilIkonaAdapter3);

        ProfilIkonaAdapter profilIkonaAdapter4=new ProfilIkonaAdapter(getContext());
        profilIkonaAdapter4.setProfileIcons(profileIcons_4level());
        profilIkonaAdapter4.setNa_voljo(false);
        profilIkonaAdapter4.setLevel(4);
        profilIkonaAdapter4.setLevel_uporabnika(levelObject.getStLevela());
        setAdapterGridView(gridView_4level,profilIkonaAdapter4);

        ProfilIkonaAdapter profilIkonaAdapter5=new ProfilIkonaAdapter(getContext());
        profilIkonaAdapter5.setProfileIcons(profileIcons_5level());
        profilIkonaAdapter5.setNa_voljo(false);
        profilIkonaAdapter5.setLevel(5);
        profilIkonaAdapter5.setLevel_uporabnika(levelObject.getStLevela());
        setAdapterGridView(gridView_5level,profilIkonaAdapter5);



        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id= uporabnik.getId();
                String ime=editText_ime.getText().toString();
                String priimek=editText_priimek.getText().toString();
                String vzdevek=editText_vzdevek.getText().toString();
                String email=uporabnik.getEmail();


                dbh.updateUporabnik(id,ime,priimek,vzdevek,email,slika_ikona_id);

                Intent mainIntent = new Intent(getActivity().getApplicationContext(), PregledNalog.class);
                startActivity(mainIntent);

            }
        });
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

 /*   @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void setAdapterGridView(GridView gridView,ProfilIkonaAdapter profilIkonaAdapter ){

        //ProfilIkonaAdapter profilIkonaAdapter=new ProfilIkonaAdapter(getContext());
        profilIkonaAdapter.setProfileIcons(profilIkonaAdapter.getProfileIcons());
        gridView.setAdapter(profilIkonaAdapter);



        if(profilIkonaAdapter.getLevel()<=profilIkonaAdapter.getLevel_uporabnika()){
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    slika_ikona_id = (int) id;
                    profileIcon.setImageResource(slika_ikona_id);

                }
            });
        }


    }



    public int[] profileIcons_1level(){
        int[] profileIcons = {
                R.drawable.pokemon_egg};
        return profileIcons;
    }

    public int[] profileIcons_2level(){
        int[] profileIcons = {
                R.drawable.green_pokeball, R.drawable.orange_pokeball,
                R.drawable.green_blue_pokeball, R.drawable.red_scar_pokeball,
                R.drawable.blueish_pokeball
        };
        return profileIcons;
    }

    public int[] profileIcons_3level(){
        int[] profileIcons = {
                R.drawable.bulbasaur1001, R.drawable.charmander1002,
                R.drawable.squirtle1003, R.drawable.pichu1004,
                R.drawable.magikarp1005,R.drawable.snorlax1006};
        return profileIcons;
    }

    public int[] profileIcons_4level(){
        int[] profileIcons = {
                R.drawable.ivysaur2001, R.drawable.charmeleon2002,
                R.drawable.wartortle2003, R.drawable.pikachu2004,
                R.drawable.gyarados2005};
        return profileIcons;
    }

    public int[] profileIcons_5level(){
        int[] profileIcons = {
                R.drawable.venusaur3001, R.drawable.charizard3002,
                R.drawable.blastoise3003,R.drawable.raichu3004};
        return profileIcons;
    }




}
