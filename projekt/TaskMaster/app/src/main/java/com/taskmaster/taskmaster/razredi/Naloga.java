package com.taskmaster.taskmaster.razredi;

import java.sql.Time;
import java.sql.Date;

/**
 * Created by Anastazia on 22. 05. 2017.
 */

public class Naloga {


    private int id;
    private int stNaloge;
    private String naziv;
    private String opis;
    private Date datumObjave;
    private Date datumOddaje;
    private Time uraOddaje;
    private double stMoznihTock;
    private double stTrenutnihTock;

    public Naloga() {

    }

    public Naloga(int stNaloge, String naziv, String opis, Date datumObjave, Date datumOddaje, Time uraOddaje,
                  double stMoznihTock) {

        this.stNaloge=stNaloge;
        this.naziv=naziv;
        this.opis=opis;
        this.datumObjave=datumObjave;
        this.datumOddaje=datumOddaje;
        this.uraOddaje=uraOddaje;
        this.stMoznihTock=stMoznihTock;

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStNaloge() {
        return stNaloge;
    }

    public void setStNaloge(int stNaloge) {
        this.stNaloge = stNaloge;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Date getDatumObjave() {
        return datumObjave;
    }

    public void setDatumObjave(Date datumObjave) {
        this.datumObjave = datumObjave;
    }

    public Date getDatumOddaje() {
        return datumOddaje;
    }

    public void setDatumOddaje(Date datumOddaje) {
        this.datumOddaje = datumOddaje;
    }

    public Time getUraOddaje() {
        return uraOddaje;
    }

    public void setUraOddaje(Time uraOddaje) {
        this.uraOddaje = uraOddaje;
    }

    public double getStMoznihTock() {
        return stMoznihTock;
    }

    public void setStMoznihTock(double stMoznihTock) {
        this.stMoznihTock = stMoznihTock;
    }

    public double getStTrenutnihTock() {
        return stTrenutnihTock;
    }

    public void setStTrenutnihTock(double stTrenutnihTock) {
        this.stTrenutnihTock = stTrenutnihTock;
    }
}
