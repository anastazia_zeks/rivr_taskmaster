package com.taskmaster.taskmaster;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ProfilIkonaAdapter extends BaseAdapter {
    private Context mContext;
    private int[] profileIcons;



    private boolean na_voljo;
    private int level;
    private int level_uporabnika;

    public ProfilIkonaAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return profileIcons.length;
    }

    public Object getItem(int position) {
        return profileIcons[position];
    }

    public long getItemId(int position) {
        return profileIcons[position];
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;
        int[] icons=getProfileIcons();


        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);



        } else {
            imageView = (ImageView) convertView;
        }



        if (isEnabled(position)) {
            imageView.setAlpha(1f);
        } else {
            imageView.setAlpha(0.45f);
        }

        imageView.setImageResource(icons[position]);
        return imageView;
    }


    public int[] getProfileIcons() {
        return profileIcons;
    }

    public void setProfileIcons(int[] profileIcons) {
        this.profileIcons = profileIcons;
    }

    public void grey_out(ImageView imageView){
        int width, height;
        height = 85;
        width = 85;
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        imageView.setImageBitmap(bmpGrayscale);
// Apply grayscale filter
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        imageView.setColorFilter(filter);
        imageView.setClickable(false);
    }




    public boolean isNa_voljo() {
        return na_voljo;
    }

    public void setNa_voljo(boolean na_voljo) {
        this.na_voljo = na_voljo;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    public int getLevel_uporabnika() {
        return level_uporabnika;
    }

    public void setLevel_uporabnika(int level_uporabnika) {
        this.level_uporabnika = level_uporabnika;
    }

    @Override
    public boolean isEnabled(int position) {

        if(level<=level_uporabnika){
            return true;
        }
        else{
            return false;
        }
    }


}