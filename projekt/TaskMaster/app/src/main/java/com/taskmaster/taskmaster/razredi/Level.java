package com.taskmaster.taskmaster.razredi;

/**
 * Created by Anastazia on 30. 05. 2017.
 */

public class Level {

    private int id;
    private String status;
    private int stTock;
    private String avatar;
    private int stLevela;
    private String nazivLevela;

    public Level() {
    }

    public Level(int id, String status, int stTock, String avatar, int stLevela, String nazivLevela) {
        this.id = id;
        this.status = status;
        this.stTock = stTock;
        this.avatar = avatar;
        this.stLevela = stLevela;
        this.nazivLevela = nazivLevela;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStTock() {
        return stTock;
    }

    public void setStTock(int stTock) {
        this.stTock = stTock;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getStLevela() {
        return stLevela;
    }

    public void setStLevela(int stLevela) {
        this.stLevela = stLevela;
    }

    public String getNazivLevela() {
        return nazivLevela;
    }

    public void setNazivLevela(String nazivLevela) {
        this.nazivLevela = nazivLevela;
    }

}
