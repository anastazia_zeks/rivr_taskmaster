package com.taskmaster.taskmaster;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.taskmaster.taskmaster.dodatki.DBHelper;
import com.taskmaster.taskmaster.parser.UrlManager;
import com.taskmaster.taskmaster.razredi.Izpit;
import com.taskmaster.taskmaster.razredi.Naloga;
import com.taskmaster.taskmaster.razredi.Predmet;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class RegistracijaUporabnik extends AppCompatActivity {

    private String fakulteta, smer, vrstaStudija, letnik, semester;
    private String ime, priimek, vzdevek, email;
    private int ikona;
    private ArrayList<Predmet> predmetiList;

    private DBHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija_uporabnik);
        dbh = new DBHelper(getApplicationContext());
        fakulteta = getIntent().getStringExtra("fakulteta");
        smer = getIntent().getStringExtra("smer");
        vrstaStudija = getIntent().getStringExtra("vrstaStudija");
        letnik = getIntent().getStringExtra("letnik");
        semester = getIntent().getStringExtra("semester");

        // parametri iz textfield
        final EditText imeValue = (EditText) findViewById(R.id.imeTextField);
        final EditText priimekValue = (EditText) findViewById(R.id.priimekTextField);
        final EditText vzdevekValue = (EditText) findViewById(R.id.vzdevekTextField);
        final EditText emailValue = (EditText) findViewById(R.id.emailTextField);

        //generiranje url
        UrlManager url = new UrlManager(fakulteta, vrstaStudija, smer, letnik, semester);
        //System.out.println(url.toString());
        new HttpAsyncTask().execute(url.toString());
        //button
        Button koncajButton = (Button) findViewById(R.id.koncajRegistracijoButton);
        koncajButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // podatki o uporabniku
                ime = imeValue.getText().toString();
                priimek = priimekValue.getText().toString();
                vzdevek = vzdevekValue.getText().toString();
                email = emailValue.getText().toString();
                ikona=2130837621;


                dbh.insertSmer(smer, fakulteta);
                dbh.insertLetnik(Integer.parseInt(letnik), Integer.parseInt(dbh.getLastID("smer")));
                dbh.insertSemester(Integer.parseInt(semester), Integer.parseInt(dbh.getLastID("letnik")));
                /**
                 * USTVARJANJE LEVELOV
                 */
                dbh.insertLevel("BEGINNER", 0, "avatar", 1, "STEP ONE");
                dbh.insertLevel("LAZY ARCHER", 99, "avatar", 2, "STEP TWO");
                dbh.insertLevel("TODO-MAN", 199, "avatar", 3, "STEP THREE");
                dbh.insertLevel("ASSIGNMENTS NINJA", 320, "avatar", 4, "STEP FOUR");
                dbh.insertLevel("TASKMASTER", 450, "avatar", 5, "STEP NO-MORE");
                dbh.insertUporabnik(ime, priimek, vzdevek, email,ikona);

                /**
                 *  POLNJENJE TIP IZPITA
                 */

                dbh.insertTipIzpita("KOLOKVIJ");
                dbh.insertTipIzpita("IZPIT");

                dbh.insertTocke(0, 0, Integer.parseInt(dbh.getLastID("uporabnik")), Integer.parseInt(dbh.getLevelIDByTocke(0)));

                for (Predmet predmet : predmetiList) {

                    /*
                    System.out.println(predmet.getNaziv());
                    System.out.println(predmet.getOpis());
                    System.out.println(predmet.getPredavatelj());
                    System.out.println(predmet.getAsistent());
                    System.out.println(predmet.getObdobjeIzvajanja());
                    System.out.println(predmet.getMozneTockeNaloge());
                    System.out.println(predmet.getMozneTockeIzpiti());
                    */

                    dbh.insertPredmet(predmet.getNaziv(), predmet.getOpis(), predmet.getPredavatelj(), predmet.getAsistent(), 0, 50, 0,
                    50, Integer.parseInt(dbh.getLastID("uporabnik")), Integer.parseInt(dbh.getLastID("semester")));

                    ArrayList<Naloga> nalogaList = predmet.getNaloge();
                    for(Naloga naloga : nalogaList) {

                        /*
                        System.out.println(naloga.getStNaloge());
                        System.out.println(naloga.getNaziv());
                        System.out.println(naloga.getOpis());
                        System.out.println(naloga.getDatumObjave());
                        System.out.println(naloga.getDatumOddaje());
                        System.out.println(naloga.getUraOddaje());
                        System.out.println(naloga.getStMoznihTock());
                        */
                        System.out.println(naloga.getDatumOddaje());

                        dbh.insertNaloga(naloga.getNaziv(), naloga.getDatumOddaje().toString(), naloga.getDatumObjave().toString(), naloga.getUraOddaje().toString(), naloga.getOpis(),
                                naloga.getStMoznihTock(), 0, Integer.parseInt(dbh.getLastID("predmet")));

                    }

                    ArrayList<Izpit> izpitList = predmet.getIzpiti();
                    for(Izpit izpit : izpitList) {

                        /*
                        System.out.println(izpit.getSt());
                        System.out.println(izpit.getTip());
                        System.out.println(izpit.getStMoznihTock());
                        System.out.println(izpit.getGradivo());
                        System.out.println(izpit.getDatumIzpita());
                        */

                        dbh.insertIzpit(izpit.getSt(), 0, izpit.getStMoznihTock(), izpit.getGradivo(), izpit.getDatumIzpita(),  Integer.parseInt(dbh.getTipIzpitaID(izpit.getTip())),  Integer.parseInt(dbh.getLastID("predmet")));

                    }

                }

                if(ime.equals(null) || priimek.equals(null) || vzdevek.equals(null) || email.equals(null)){

                    Toast.makeText(getBaseContext(), "Izpolnite vse podatke!", Toast.LENGTH_LONG).show();

                } else {

                    Intent pregledNalog = new Intent(v.getContext(), PregledNalog.class);
                    pregledNalog.putExtra("vzdevek", vzdevek);
                    pregledNalog.putExtra("email", email);
                    startActivity(pregledNalog);

                }
            }

        });
    }

    ArrayList<Predmet> getElementsFromJsonFile(JSONObject jsonObject) throws Exception{
        ArrayList<Predmet> predmetiList = new ArrayList<Predmet>();

//		System.out.println(celotenJsonFile);

        JSONObject semester = jsonObject;
        JSONArray semesterData = semester.getJSONArray("semester");

        for (int i = 0; i < semesterData.length() ;i++) {

            ArrayList<Naloga> nalogaList = new ArrayList<Naloga>();
            ArrayList<Izpit> izpitiList = new ArrayList<Izpit>();
            Predmet predmetObj = new Predmet();

            DateFormat formatterTime = new SimpleDateFormat("HH:mm");
            DateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");

            JSONObject predmet = (JSONObject) semesterData.get(i);
            JSONArray predmetData = predmet.getJSONArray("predmet");

            /**
             * INFORMACIJE O PREDMETU
             */

            JSONObject informacije = (JSONObject) predmetData.get(0);
            JSONArray informacijeData = informacije.getJSONArray("informacije");

            // izpis informacij
//			System.out.println(informacijeData.get(0));

            JSONObject informacijeParts = (JSONObject) informacijeData.get(0);

            String nazivPredmeta = informacijeParts.getString("naziv");
            String opisPredmeta = informacijeParts.getString("opis");
            String izvajanjePredmeta = informacijeParts.getString("obdobje_izvajanja");
            String izvajateljPredmeta = informacijeParts.getString("izvajatelj");
            String asistentPredmeta = informacijeParts.getString("asistent");

            predmetObj.setNaziv(nazivPredmeta);
            predmetObj.setOpis(opisPredmeta);
            predmetObj.setObdobjeIzvajanja(izvajanjePredmeta);
            predmetObj.setPredavatelj(izvajateljPredmeta);
            predmetObj.setAsistent(asistentPredmeta);

//			System.out.println(asistentPredmeta);

            /**
             *  VAJE
             */

            JSONObject vaje = (JSONObject) predmetData.get(1);
            JSONArray vajeData = vaje.getJSONArray("vaje");

            // izpis vaje
//			System.out.println(vajeData.get(0));

            JSONObject naloge = (JSONObject) vajeData.get(0);
            JSONArray nalogeData = naloge.getJSONArray("naloge");

            for (int j = 0; j < nalogeData.length() ;j++) {

                // naloga 1
//				System.out.println(nalogeData.get(j));


                JSONObject nalogeParts = (JSONObject) nalogeData.get(j);

				//System.out.println(nalogeParts.get("naloga"));

                int stevilkaNaloge = Integer.parseInt(nalogeParts.getString("naloga"));
                String nazivNaloge = nalogeParts.getString("naziv");
                String opisNaloge = nalogeParts.getString("opis");
                Date datumObjaveNaloge =  new java.sql.Date(formatterDate.parse(nalogeParts.getString("datum_objave")).getTime());
                Date datumOddajeNaloge = new java.sql.Date(formatterDate.parse(nalogeParts.getString("datum_oddaje")).getTime());
                Time uraOddajeNaloge =  new java.sql.Time(formatterTime.parse(nalogeParts.getString("ura_oddaje")).getTime());
                Double steviloTockNaloge = (double) Integer.parseInt(nalogeParts.getString("stevilo_tock"));

                nalogaList.add(new Naloga(stevilkaNaloge, nazivNaloge, opisNaloge, datumObjaveNaloge, datumOddajeNaloge, uraOddajeNaloge, steviloTockNaloge));

            }

            predmetObj.setNaloge(nalogaList);

            /**
             * PREDAVANJA
             */

            JSONObject predavanja = (JSONObject) predmetData.get(2);
            JSONArray predavanjaData = predavanja.getJSONArray("predavanja");

            // izpis vaje
//			System.out.println(predavanjaData.get(0));

            JSONObject vmesniIzpiti = (JSONObject) predavanjaData.get(0);
            JSONArray vmesniIzpitiData = vmesniIzpiti.getJSONArray("vmesni_izpiti");

            for (int j = 0; j < vmesniIzpitiData.length() ;j++) {

                // izpis vmesni izpiti
//				System.out.println(vmesniIzpitiData.get(j));

                JSONObject vmesniIzpitiParts = (JSONObject) vmesniIzpitiData.get(j);

                int stevilkaVmesnegaIzpita = Integer.parseInt(vmesniIzpitiParts.getString("kolokvij"));
                String gradivoVmesnegaIzpita = vmesniIzpitiParts.getString("gradivo");
                Date datumVmesnegaIzpita = new java.sql.Date(formatterDate.parse(vmesniIzpitiParts.getString("datum")).getTime());
                double steviloTockVmesnegaIzpita = (double) Integer.parseInt(vmesniIzpitiParts.getString("stevilo_tock"));

                izpitiList.add(new Izpit(stevilkaVmesnegaIzpita, "KOLOKVIJ", steviloTockVmesnegaIzpita, gradivoVmesnegaIzpita, datumVmesnegaIzpita));


            }

            JSONObject izpiti = (JSONObject) predavanjaData.get(1);
            JSONArray izpitiData = izpiti.getJSONArray("izpiti");

            for (int j = 0; j < izpitiData.length() ;j++) {

                // izpis izpiti
//				System.out.println(izpitiData.get(j));

                JSONObject izpitiParts = (JSONObject) izpitiData.get(j);

                int stevilkaIzpita = Integer.parseInt(izpitiParts.getString("izpit"));
                String gradivoIzpita = izpitiParts.getString("gradivo");
                Date datumIzpita = new java.sql.Date(formatterDate.parse(izpitiParts.getString("datum")).getTime());
                double steviloTockIzpita = (double) Integer.parseInt(izpitiParts.getString("stevilo_tock"));

                izpitiList.add(new Izpit(stevilkaIzpita, "IZPIT", steviloTockIzpita, gradivoIzpita, datumIzpita));



            }

            predmetObj.setIzpiti(izpitiList);

            predmetiList.add(predmetObj);

        }

        return predmetiList;
    }

    /**
     * Preverja če je prišlo do povezave ali pa ni
     * @return boolern
     */

    public boolean isConnected(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     *
     * @param url
     * @return
     */
    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result +=  new String(line.getBytes("ISO-8859-1"), StandardCharsets.UTF_8);

        inputStream.close();
        return result;

    }
    /**
     * Nova nit in napolni json Object
     */
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getBaseContext(), "Podatki so bili uzpešno sprejeti.", Toast.LENGTH_LONG).show();

            try {

                JSONObject jsonObject = new JSONObject(result);
                predmetiList = getElementsFromJsonFile(jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
