package com.taskmaster.taskmaster.razredi;

import java.sql.Date;

/**
 * Created by Anastazia on 22. 05. 2017.
 */

public class Izpit {

    private int st;
    private int id;
    private String tip;
    private double stMoznihTock;
    private double stTrenutnihTock;
    private String gradivo;
    private Date datumIzpita;

    public Izpit() {
    }

    public Izpit(int st, String tip, double stMoznihTock, String gradivo, Date datumIzpita) {
        this.st = st;
        this.tip = tip;
        this.stMoznihTock = stMoznihTock;
        this.gradivo = gradivo;
        this.datumIzpita = datumIzpita;
    }

    public int getSt() {
        return st;
    }

    public void setSt(int st) {
        this.st = st;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public double getStMoznihTock() {
        return stMoznihTock;
    }

    public void setStMoznihTock(double stMoznihTock) {
        this.stMoznihTock = stMoznihTock;
    }

    public String getGradivo() {
        return gradivo;
    }

    public void setGradivo(String gradivo) {
        this.gradivo = gradivo;
    }

    public Date getDatumIzpita() {
        return datumIzpita;
    }

    public void setDatumIzpita(Date datumIzpita) {
        this.datumIzpita = datumIzpita;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getStTrenutnihTock() {
        return stTrenutnihTock;
    }

    public void setStTrenutnihTock(double stTrenutnihTock) {
        this.stTrenutnihTock = stTrenutnihTock;
    }
}
