package com.taskmaster.taskmaster.dodatki;

/**
 * Created by Anastazia on 29. 05. 2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.taskmaster.taskmaster.razredi.Izpit;
import com.taskmaster.taskmaster.razredi.Level;
import com.taskmaster.taskmaster.razredi.Naloga;
import com.taskmaster.taskmaster.razredi.Uporabnik;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    // IME BAZE
    public static final String DATABASE_NAME = "/data/data/com.taskmaster.taskmaster/databases/taskmaster.sqlite";
    // VERZIJA BAZE
    private static final int DATABASE_VERSION = 38;
    // IMENA TABEL
    public static final String LEVEL_TABLE_NAME = "level";
    public static final String TOCKE_TABLE_NAME = "tocke";
    public static final String UPORABNIK_TABLE_NAME = "uporabnik";
    public static final String PREDMET_TABLE_NAME = "predmet";
    public static final String NALOGA_TABLE_NAME = "naloga";
    public static final String IZPIT_TABLE_NAME = "izpit";
    public static final String TIP_IZPITA_TABLE_NAME = "tip_izpita";
    public static final String SEMESTER_TABLE_NAME = "semester";
    public static final String LETNIK_TABLE_NAME = "letnik";
    public static final String SMER_TABLE_NAME = "smer";

    // LEVEL -> ATRIBUTI
    public static final String ID_LEVEL = "_idlevel";
    public static final String STATUS = "status";
    public static final String ST_TOCK = "st_tock";
    public static final String AVATAR = "avatar";
    public static final String ST_LEVELA = "st_levela";
    public static final String NAZIV_LEVELA = "naziv_levela";

    // TOCKE -> ATRIBUTI
    public static final String ID_TOCKE= "_idtocke";
    public static final String ST_MOZNIH_TOCK_TOCKE = "st_moznih_tock_tocke";
    public static final String ST_TRENUTNIH_TOCK_TOCKE = "st_trenutnih_tock_tocke";
    // TOCKE -> TUJI KLJUČI
    public static final String FK_TOCKE_IDUPORABNIK = "fk_tocke_iduporabnik";
    public static final String FK_TOCKE_IDLEVEL = "fk_tocke_idlevel";

    // UPORABNIK -> ATRIBUTI
    public static final String ID_UPORABNIK = "_iduporabnik";
    public static final String VZDEVEK = "vzdevek";
    public static final String IME = "ime";
    public static final String PRIIMEK = "priimek";
    public static final String EMAIL = "email";
    public static final String IKONA="ikona";

    // PREDMET -> ATRIBUTI
    public static final String ID_PREDMET = "_idpredmet";
    public static final String PREDAVATELJ = "predavatelj";
    public static final String ASISTENT = "asistent";
    public static final String TRENUTNE_TOCKE_NALOGE = "trenutne_tocke_naloge";
    public static final String MOZNE_TOCKE_NALOGE = "mozne_tocke_naloge";
    public static final String TRENUTNE_TOCKE_IZPITI = "trenutne_tocke_izpiti";
    public static final String MOZNE_TOCKE_IZPITI = "mozne_tocke_izpiti";
    public static final String NAZIV_PREDMET = "naziv_predmet";
    public static final String OPIS_PREDMET = "opis_predmet";
    // PREDMET -> TUJI KLJUČI
    public static final String FK_PREDMET_IDSEMESTER = "fk_predmet_idsemester";
    public static final String FK_PREDMET_IDUPORABNIK = "fk_predmet_iduporabnik";

    // NALOGA -> ATRIBUTI
    public static final String ID_NALOGA = "_idnaloga";
    public static final String DATUM_ODDAJE = "datum_oddaje";
    public static final String DATUM_OBJAVE = "datum_objave";
    public static final String URA_ODDAJE = "ura_oddaje";
    public static final String ST_MOZNIH_TOCK_NALOGA = "st_moznih_tock_naloga";
    public static final String ST_TRENUTNIH_TOCK_NALOGA = "st_trenutnih_tock_naloga";
    public static final String NAZIV_NALOGA = "naziv_naloga";
    public static final String OPIS_NALOGA = "opis_naloga";
    // NALOGA -> TUJI KLJUČI
    public static final String FK_NALOGA_IDPREDMET = "fk_naloga_idpredmet";

    // IZPIT -> ATRIBUTI
    public static final String ID_IZPIT = "_idizpit";
    public static final String ST_IZPITA = "st_izpita";
    public static final String GRADIVO = "gradivo";
    public static final String DATUM_IZPITA = "datum_izpita";
    public static final String ST_MOZNIH_TOCK_IZPIT = "st_moznih_tock_izpit";
    public static final String ST_TRENUTNIH_TOCK_IZPIT = "st_trenutnih_tock_izpit";
    // IZPIT -> TUJI KLJUČI
    public static final String FK_IZPIT_IDPREDMET = "fk_izpit_idpredmet";
    public static final String FK_IZPIT_IDTIP_IZPITA = "fk_izpit_idtip_izpita";

    // TIP_IZPITA -> ATRIBUTI
    // id
    public static final String ID_TIP_IZPITA = "_idtip_izpita";
    public static final String TIP = "tip";

    // SEMESTER -> ATRIBUTI
    // id
    public static final String ID_SEMESTER = "_idsemester";
    public static final String SEMESTER = "semester";
    // SEMESTER -> TUJI KLJUČI
    public static final String FK_SEMESTER_IDLETNIK = "fk_semester_idletnik";

    // LETNIK -> ATRIBUTI
    public static final String ID_LETNIK = "_idletnik";
    public static final String LETNIK = "letnik";
    // LETNIK -> TUJI KLJUČI
    public static final String FK_LETNIK_IDSMER = "fk_letnik_idsmer";

    // SMER -> ATRIBUTI
    // id, opis, naziv
    public static final String ID_SMER = "_idsmer";
    public static final String NAZIV_SMER = "naziv_smer";
    public static final String OPIS_SMER = "opis_smer";

    // CREATE TABLES
    // create level
    private static final String CREATE_TABLE_LEVEL = "CREATE TABLE "
            + LEVEL_TABLE_NAME + "(" + ID_LEVEL + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + STATUS + " TEXT," + ST_TOCK + " INTEGER,"
            + AVATAR + " TEXT," + ST_LEVELA + " INTEGER," + NAZIV_LEVELA + " TEXT" + ");";

    // create uporabnik
    private static final String CREATE_TABLE_UPORABNIK = "CREATE TABLE "
            + UPORABNIK_TABLE_NAME + "(" + ID_UPORABNIK + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + VZDEVEK + " TEXT," + IME + " TEXT,"
            + PRIIMEK + " TEXT," + EMAIL + " TEXT,"+IKONA+" INTEGER"+ ");";

    // create smer
    private static final String CREATE_TABLE_SMER = "CREATE TABLE "
            + SMER_TABLE_NAME + "(" + ID_SMER + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAZIV_SMER + " TEXT," + OPIS_SMER + " TEXT" + ");";

    // create tip izpita
    private static final String CREATE_TABLE_TIP_IZPITA = "CREATE TABLE "
            + TIP_IZPITA_TABLE_NAME + "(" + ID_TIP_IZPITA + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TIP + " TEXT" + ");";

    // create tocke
    private static final String CREATE_TABLE_TOCKE = "CREATE TABLE "
            + TOCKE_TABLE_NAME + "(" + ID_TOCKE + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ST_MOZNIH_TOCK_TOCKE + " INTEGER,"
            + FK_TOCKE_IDLEVEL + " INTEGER,"
            + FK_TOCKE_IDUPORABNIK + " INTEGER,"
            + ST_TRENUTNIH_TOCK_TOCKE + " INTEGER, FOREIGN KEY("
            + FK_TOCKE_IDUPORABNIK + ") REFERENCES " + UPORABNIK_TABLE_NAME + "(" + ID_UPORABNIK + "), FOREIGN KEY("
            + FK_TOCKE_IDLEVEL + ") REFERENCES " + LEVEL_TABLE_NAME + "(" + ID_LEVEL + "));";

    // create naloga
    private static final String CREATE_TABLE_NALOGA = "CREATE TABLE "
            + NALOGA_TABLE_NAME + "(" + ID_NALOGA + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAZIV_NALOGA + " TEXT,"
            + FK_NALOGA_IDPREDMET + " INTEGER,"
            + DATUM_ODDAJE + " DATE," + DATUM_OBJAVE + " DATE,"
            + URA_ODDAJE + " TIME," + OPIS_NALOGA + " TEXT," + ST_MOZNIH_TOCK_NALOGA + " INTEGER,"
            + ST_TRENUTNIH_TOCK_NALOGA + " INTEGER, FOREIGN KEY("
            + FK_NALOGA_IDPREDMET + ") REFERENCES " + PREDMET_TABLE_NAME + "(" + ID_PREDMET + "));";

    // create izpit
    private static final String CREATE_TABLE_IZPIT = "CREATE TABLE "
            + IZPIT_TABLE_NAME + "(" + ID_IZPIT + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ST_IZPITA + " INTEGER,"
            + FK_IZPIT_IDPREDMET + " INTEGER,"
            + FK_IZPIT_IDTIP_IZPITA + " INTEGER,"
            + ST_MOZNIH_TOCK_IZPIT + " INTEGER," + ST_TRENUTNIH_TOCK_IZPIT + " INTEGER,"
            + GRADIVO + " TEXT," + DATUM_IZPITA + " DATE, FOREIGN KEY("
            + FK_IZPIT_IDPREDMET + ") REFERENCES " + PREDMET_TABLE_NAME + "(" + ID_PREDMET + "), FOREIGN KEY("
            + FK_IZPIT_IDTIP_IZPITA + ") REFERENCES " + TIP_IZPITA_TABLE_NAME + "(" + ID_TIP_IZPITA + "));";

    // create predmet
    private static final String CREATE_TABLE_PREDMET = "CREATE TABLE "
            + PREDMET_TABLE_NAME + "(" + ID_PREDMET + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAZIV_PREDMET + " TEXT,"
            + FK_PREDMET_IDSEMESTER + " INTEGER, "
            + FK_PREDMET_IDUPORABNIK + " INTEGER, "
            + OPIS_PREDMET+ " TEXT," + PREDAVATELJ + " TEXT,"
            + ASISTENT + " TEXT," + TRENUTNE_TOCKE_IZPITI + " INTEGER,"
            + MOZNE_TOCKE_IZPITI + " INTEGER," + TRENUTNE_TOCKE_NALOGE + " INTEGER,"
            + MOZNE_TOCKE_NALOGE + " INTEGER, FOREIGN KEY("
            + FK_PREDMET_IDSEMESTER + ") REFERENCES " + SEMESTER_TABLE_NAME + "(" + ID_SEMESTER + "), FOREIGN KEY("
            + FK_PREDMET_IDUPORABNIK + ") REFERENCES " + UPORABNIK_TABLE_NAME + "(" + ID_UPORABNIK + "));";

    // create predmet
    private static final String CREATE_TABLE_SEMESTER = "CREATE TABLE "
            + SEMESTER_TABLE_NAME + "(" + ID_SEMESTER + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FK_SEMESTER_IDLETNIK + " INTEGER, "
            + SEMESTER + " INTEGER, FOREIGN KEY("
            + FK_SEMESTER_IDLETNIK + ") REFERENCES " + LETNIK_TABLE_NAME + "(" + ID_LETNIK + "));";

    // create letnik
    private static final String CREATE_TABLE_LETNIK = "CREATE TABLE "
            + LETNIK_TABLE_NAME + "(" + ID_LETNIK + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FK_LETNIK_IDSMER + " INTEGER, "
            + LETNIK + " INTEGER, FOREIGN KEY("
            + FK_LETNIK_IDSMER + ") REFERENCES " + SMER_TABLE_NAME + "(" + ID_SMER + "));";

    private HashMap hp;

    private DateFormat formatterTime = new SimpleDateFormat("HH:mm");
    private DateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL("PRAGMA foreign_keys = ON;");
        db.execSQL(CREATE_TABLE_SMER);
        db.execSQL(CREATE_TABLE_LETNIK);
        db.execSQL(CREATE_TABLE_SEMESTER);
        db.execSQL(CREATE_TABLE_LEVEL);
        db.execSQL(CREATE_TABLE_UPORABNIK);
        db.execSQL(CREATE_TABLE_TOCKE);
        db.execSQL(CREATE_TABLE_TIP_IZPITA);
        db.execSQL(CREATE_TABLE_PREDMET);
        db.execSQL(CREATE_TABLE_NALOGA);
        db.execSQL(CREATE_TABLE_IZPIT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + SMER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + LETNIK_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SEMESTER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + LEVEL_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + UPORABNIK_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TIP_IZPITA_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TOCKE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PREDMET_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NALOGA_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + IZPIT_TABLE_NAME);
        onCreate(db);
    }

    /*
        INSERTS
     */
    public boolean insertUporabnik (String ime, String priimek, String vzdevek, String email,int ikona) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IME, ime);
        contentValues.put(PRIIMEK, priimek);
        contentValues.put(VZDEVEK, vzdevek);
        contentValues.put(EMAIL, email);
        contentValues.put(IKONA,ikona);
        db.insert(UPORABNIK_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertLevel (String status, int st_tock, String avatar, int st_levela, String naziv_levela) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STATUS, status);
        contentValues.put(ST_TOCK, st_tock);
        contentValues.put(AVATAR, avatar);
        contentValues.put(ST_LEVELA, st_levela);
        contentValues.put(NAZIV_LEVELA, naziv_levela);
        db.insert(LEVEL_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertTipIzpita (String tip_izpita) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIP, tip_izpita);
        db.insert(TIP_IZPITA_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertSmer (String naziv, String opis) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAZIV_SMER, naziv);
        contentValues.put(OPIS_SMER, opis);
        db.insert(SMER_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertLetnik (int letnik, int fk_letnik_idsmer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LETNIK, letnik);
        contentValues.put(FK_LETNIK_IDSMER, fk_letnik_idsmer);
        db.insert(LETNIK_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertSemester (int semester, int fk_semester_idletnik) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SEMESTER, semester);
        contentValues.put(FK_SEMESTER_IDLETNIK, fk_semester_idletnik);
        db.insert(SEMESTER_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertTocke (double st_moznih_tock, double st_trenutnih_tock, int fk_tocke_iduporabnik , int fk_tocke_idlevel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ST_MOZNIH_TOCK_TOCKE, st_moznih_tock);
        contentValues.put(ST_TRENUTNIH_TOCK_TOCKE, st_trenutnih_tock);
        contentValues.put(FK_TOCKE_IDUPORABNIK, fk_tocke_iduporabnik);
        contentValues.put(FK_TOCKE_IDLEVEL, fk_tocke_idlevel);
        db.insert(TOCKE_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertNaloga (String naziv, String datum_oddaje, String datum_objave, String ura_oddaje, String opis, double st_moznih_tock, int st_trenutnih_tock, int fk_naloga_idpredmet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAZIV_NALOGA, naziv);
        contentValues.put(DATUM_ODDAJE, datum_oddaje);
        contentValues.put(DATUM_OBJAVE, datum_objave);
        contentValues.put(URA_ODDAJE, ura_oddaje);
        contentValues.put(OPIS_NALOGA, opis);
        contentValues.put(ST_MOZNIH_TOCK_NALOGA, st_moznih_tock);
        contentValues.put(ST_TRENUTNIH_TOCK_NALOGA, st_trenutnih_tock);
        contentValues.put(FK_NALOGA_IDPREDMET, fk_naloga_idpredmet);
        db.insert(NALOGA_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertPredmet (String naziv, String opis, String predavatelj, String asistent, int trenutne_tocke_naloge, int mozne_tocke_naloge, int trenutne_tocke_izpiti,
                                  int mozne_tocke_izpiti, int fk_predmet_iduporabnik, int fk_predmet_idsemester) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAZIV_PREDMET, naziv);
        contentValues.put(OPIS_PREDMET, opis);
        contentValues.put(PREDAVATELJ, predavatelj);
        contentValues.put(ASISTENT, asistent);
        contentValues.put(TRENUTNE_TOCKE_NALOGE, trenutne_tocke_naloge);
        contentValues.put(MOZNE_TOCKE_NALOGE, mozne_tocke_naloge);
        contentValues.put(TRENUTNE_TOCKE_IZPITI, trenutne_tocke_izpiti);
        contentValues.put(MOZNE_TOCKE_IZPITI, mozne_tocke_izpiti);
        contentValues.put(FK_PREDMET_IDUPORABNIK, fk_predmet_iduporabnik);
        contentValues.put(FK_PREDMET_IDSEMESTER, fk_predmet_idsemester);
        db.insert(PREDMET_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertIzpit (int st_izpita, double st_trenutnih_tock, double st_moznih_tock, String gradivo, Date datum_izpita, int fk_izpit_idtip_izpita, int fk_izpit_idpredmet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ST_IZPITA, st_izpita);
        contentValues.put(ST_TRENUTNIH_TOCK_IZPIT, st_trenutnih_tock);
        contentValues.put(ST_MOZNIH_TOCK_IZPIT, st_moznih_tock);
        contentValues.put(GRADIVO, gradivo);
        contentValues.put(DATUM_IZPITA, String.valueOf(datum_izpita));
        contentValues.put(FK_IZPIT_IDTIP_IZPITA, fk_izpit_idtip_izpita);
        contentValues.put(FK_IZPIT_IDPREDMET, fk_izpit_idpredmet);
        db.insert(IZPIT_TABLE_NAME, null, contentValues);
        return true;
    }

    /*
        GET LAST ID
     */

    public String getLastID(String tablename) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM " + tablename + " ORDER BY _id" + tablename + " DESC LIMIT 1;", null);
        String temp = null;

        if (res.moveToFirst()) {
            temp = res.getString(res.getColumnIndex("_id" + tablename));
        }
        return temp;
    }
    /*
        GET LAST UPORABNIK
     */

    public Uporabnik getLastUporabnik() {

        Uporabnik uporabnik = new Uporabnik();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM " + UPORABNIK_TABLE_NAME + " ORDER BY _id" + UPORABNIK_TABLE_NAME + " DESC LIMIT 1;", null);

        if (res.moveToFirst()) {
            uporabnik.setId(Integer.parseInt(res.getString(res.getColumnIndex("_id" + UPORABNIK_TABLE_NAME))));
            uporabnik.setIme(res.getString(res.getColumnIndex(IME)));
            uporabnik.setPriimek(res.getString(res.getColumnIndex(PRIIMEK)));
            uporabnik.setVzdevek(res.getString(res.getColumnIndex(VZDEVEK)));
            uporabnik.setEmail(res.getString(res.getColumnIndex(EMAIL)));
            uporabnik.setIkona(Integer.parseInt(res.getString(res.getColumnIndex(IKONA))));


        }
        return uporabnik;
    }

    /*
        GET ID LEVELA GLEDE NA ST TOCK
     */


    public String getLevelIDByTocke (double trenutneTocke) {

        int lvlTocke = 0;

        if(trenutneTocke < 99){

            lvlTocke = 0;

        }else if(trenutneTocke >= 99 && trenutneTocke < 199){

            lvlTocke = 99;

        }else if(trenutneTocke >= 199 && trenutneTocke < 320){

            lvlTocke = 199;

        }else if(trenutneTocke >= 320 && trenutneTocke < 450){

            lvlTocke = 320;

        }else if(trenutneTocke >= 450){

            lvlTocke = 450;

        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT _id" + LEVEL_TABLE_NAME + " FROM " + LEVEL_TABLE_NAME + " WHERE " + ST_TOCK + "=" + lvlTocke + " LIMIT 1;", null);
        String temp = null;

        if (res.moveToFirst()) {
            temp = res.getString(res.getColumnIndex("_id" + LEVEL_TABLE_NAME));
        }
        return temp;
    }

    public Level getLevel (String id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT *  FROM " + LEVEL_TABLE_NAME + " WHERE " + ID_LEVEL + "=" + id + " LIMIT 1;", null);

        Level level=new Level();


        if (res.moveToFirst()) {
            level.setStatus(res.getString(res.getColumnIndex(STATUS)));
            level.setNazivLevela(res.getString(res.getColumnIndex(NAZIV_LEVELA)));
            level.setStLevela(Integer.parseInt(res.getString(res.getColumnIndex(ST_LEVELA))));
            level.setStTock(res.getColumnIndex(ST_TOCK));

        }
        return level;
    }



    /*
        GET ID Z DEFINIRANEGA TIPA IZPITA
     */


    public String getTipIzpitaID (String tipIzpita) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT _id" + TIP_IZPITA_TABLE_NAME + " FROM " + TIP_IZPITA_TABLE_NAME + " WHERE " + TIP + "='" + tipIzpita + "' LIMIT 1;", null);
        String temp = null;

        if (res.moveToFirst()) {
            temp = res.getString(res.getColumnIndex("_id" + TIP_IZPITA_TABLE_NAME));
        }
        return temp;
    }

    /**
     *  Z TABELE BERE TUJI KLJUČ PREDMETA
     * @param id
     * @return
     */

    public int getPredmetID(int id, String tablename) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  null;
        String temp = null;

        if (tablename.equals("izpit")){
            res = db.rawQuery("SELECT " + FK_IZPIT_IDPREDMET + " FROM " + IZPIT_TABLE_NAME + " WHERE " + ID_IZPIT + "=" + id + " LIMIT 1;", null);

            if (res.moveToFirst()) {
                temp = res.getString(res.getColumnIndex(FK_IZPIT_IDPREDMET));
            }
        }else if(tablename.equals("naloga")){
            res =  db.rawQuery("SELECT " + FK_NALOGA_IDPREDMET + " FROM " + NALOGA_TABLE_NAME + " WHERE " + ID_NALOGA + "=" + id + " LIMIT 1;", null);

            if (res.moveToFirst()) {
                temp = res.getString(res.getColumnIndex(FK_NALOGA_IDPREDMET));
            }

        }


        return Integer.parseInt(temp);
    }

    public double getTrenutneTockeForPredmet(int idPredmet, String tablename){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  null;

        double stTrenutnihTock = 0;

        if(tablename.equals("naloga")){
            res = db.rawQuery("SELECT " + ST_TRENUTNIH_TOCK_NALOGA + " FROM " + NALOGA_TABLE_NAME + " WHERE " + FK_NALOGA_IDPREDMET + "=" + idPredmet + ";", null);

            res.moveToFirst();
            while(res.isAfterLast() == false){

                stTrenutnihTock += Double.parseDouble(res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA)));

                res.moveToNext();
            }

        }else if (tablename.equals("izpit")){
            res = db.rawQuery("SELECT " + ST_TRENUTNIH_TOCK_IZPIT + " FROM " + IZPIT_TABLE_NAME + " WHERE " + FK_IZPIT_IDPREDMET + "=" + idPredmet + ";", null);

            res.moveToFirst();
            while(res.isAfterLast() == false){

                stTrenutnihTock += Double.parseDouble(res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT)));

                res.moveToNext();
            }

        }

        return stTrenutnihTock;
    }

    /**
     * GET ST NALOG
     * @param oddane
     * @return
     */
    public int getStNalog (boolean oddane) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = null;
        int st = 0;

        if(oddane){
            res =  db.rawQuery("SELECT _id" + NALOGA_TABLE_NAME + " FROM " + NALOGA_TABLE_NAME + " WHERE " + ST_TRENUTNIH_TOCK_NALOGA + "> 0 ;", null);
        }else {
            res =  db.rawQuery("SELECT _id" + NALOGA_TABLE_NAME + " FROM " + NALOGA_TABLE_NAME + " WHERE " + ST_TRENUTNIH_TOCK_NALOGA + "= 0 ;", null);
        }


        res.moveToFirst();
        while (res.isAfterLast() == false) {

            st++;

            res.moveToNext();
        }

        return st;
    }
    public int getStIzpitov (boolean oddane) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = null;
        int st = 0;

        if(oddane){
            res =  db.rawQuery("SELECT _id" + IZPIT_TABLE_NAME + " FROM " + IZPIT_TABLE_NAME + " WHERE " + ST_TRENUTNIH_TOCK_IZPIT + "> 0 ;", null);
        }else {
            res =  db.rawQuery("SELECT _id" + IZPIT_TABLE_NAME + " FROM " + IZPIT_TABLE_NAME + " WHERE " + ST_TRENUTNIH_TOCK_IZPIT + "= 0 ;", null);
        }


        res.moveToFirst();
        while (res.isAfterLast() == false) {

            st++;

            res.moveToNext();
        }

        return st;
    }
    /**
     *  METODA SEŠTEVA VSE VREDNOSTI OD TABELE PREDMET - GLEDE NA PARAMETER MOZNE ALI TRENUTNE TOČKE ZA VSE PREDMETE
     * @param idUporabnik, ker beremo točke z uporabnika
     * @param vrstaTock je lahko trenutne ali mozne
     * @return
     */
    public double getTockeForTocke(int idUporabnik, String vrstaTock){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  null;

        double tocke = 0;

        res = db.rawQuery("SELECT * FROM " + PREDMET_TABLE_NAME + " WHERE " + FK_PREDMET_IDUPORABNIK + "=" + idUporabnik + ";", null);

        if (vrstaTock.equals("trenutne")) {
            res.moveToFirst();
            while (res.isAfterLast() == false) {

                tocke += Double.parseDouble(res.getString(res.getColumnIndex(TRENUTNE_TOCKE_NALOGE)));
                tocke += Double.parseDouble(res.getString(res.getColumnIndex(TRENUTNE_TOCKE_IZPITI)));

                res.moveToNext();
            }

        }else if (vrstaTock.equals("mozne")){
            res.moveToFirst();
            while (res.isAfterLast() == false) {

                tocke += Double.parseDouble(res.getString(res.getColumnIndex(MOZNE_TOCKE_NALOGE)));
                tocke += Double.parseDouble(res.getString(res.getColumnIndex(MOZNE_TOCKE_IZPITI)));
                System.out.println("TOCKE>>>"+tocke);
                res.moveToNext();
            }

        }

        return tocke;
    }
    /*
        UPDATES

     */

    public boolean updateUporabnik (int id, String ime, String priimek, String vzdevek, String email,int ikona) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IME, ime);
        contentValues.put(PRIIMEK, priimek);
        contentValues.put(VZDEVEK, vzdevek);
        contentValues.put(EMAIL, email);
        contentValues.put(IKONA,ikona);
        db.update(UPORABNIK_TABLE_NAME, contentValues,  ID_UPORABNIK+" = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean updateTocke (int id, double st_moznih_tock, double st_trenutnih_tock, int fk_tocke_idlevel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ST_MOZNIH_TOCK_TOCKE, st_moznih_tock);
        contentValues.put(ST_TRENUTNIH_TOCK_TOCKE, st_trenutnih_tock);
        contentValues.put(FK_TOCKE_IDLEVEL, fk_tocke_idlevel);
        db.update(TOCKE_TABLE_NAME, contentValues,  ID_TOCKE+" = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean updateNaloga (int id, double st_trenutnih_tock) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ST_TRENUTNIH_TOCK_NALOGA, st_trenutnih_tock);
        db.update(NALOGA_TABLE_NAME, contentValues, ID_NALOGA+" = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean updateIzpit (int id, double st_trenutnih_tock) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ST_TRENUTNIH_TOCK_IZPIT, st_trenutnih_tock);
        db.update(IZPIT_TABLE_NAME, contentValues,  ID_IZPIT+" = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean updatePredmet (int id, double tocke, String tablename) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        if(tablename.equals("naloga")){
            contentValues.put(TRENUTNE_TOCKE_NALOGE, tocke);
        }else if(tablename.equals("izpit")) {
            contentValues.put(TRENUTNE_TOCKE_IZPITI, tocke);
        }

        db.update(PREDMET_TABLE_NAME, contentValues,  ID_PREDMET+" = ? ", new String[] { Integer.toString(id) } );
        return true;
    }
    /**
      *  GET NALOGE
     **/

    public ArrayList<ArrayList<Object>> getAllNaloge(boolean nalogeZaNazaj, String datumDo) {

        ArrayList<ArrayList<Object>> listNaloge=new ArrayList<ArrayList<Object>>();

        String[] temp = new String[numberOfRows(NALOGA_TABLE_NAME)];
        int i = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = null;

        Date currentDate = new Date();
        String danasnjiDatum = formatterDate.format(currentDate);

        res =  db.rawQuery("SELECT " + NAZIV_PREDMET + ", " + OPIS_PREDMET + ", " + PREDAVATELJ + ", " + ASISTENT + "," + MOZNE_TOCKE_NALOGE + ", " + MOZNE_TOCKE_IZPITI + ", "
                        + NAZIV_NALOGA + "," + DATUM_ODDAJE + ", " + DATUM_OBJAVE + ", " + URA_ODDAJE + ", " + OPIS_NALOGA + ", " + ST_MOZNIH_TOCK_NALOGA + "," + ST_TRENUTNIH_TOCK_NALOGA + "," + FK_NALOGA_IDPREDMET + " "
                        +"  FROM " + PREDMET_TABLE_NAME + " INNER JOIN " + NALOGA_TABLE_NAME
                        + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + NALOGA_TABLE_NAME + "." + FK_NALOGA_IDPREDMET + " ORDER BY date(" + DATUM_ODDAJE + ") ASC;", null );



        res.moveToFirst();
        while(res.isAfterLast() == false){

            ArrayList<Object> naloga=new ArrayList<Object>();

            if (nalogeZaNazaj){

                if (!StringUtils.isEmpty(datumDo)){

                    if (!(java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(datumDo)))) {

                        temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(NAZIV_NALOGA))
                                + " (" + res.getString(res.getColumnIndex(DATUM_ODDAJE)) + ") TOCKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                                + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));
                        naloga.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                        naloga.add(1,temp[i]);
                        listNaloge.add(naloga);
                        i++;

                    }

                }else {

                    temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(NAZIV_NALOGA))
                            + " (" + res.getString(res.getColumnIndex(DATUM_ODDAJE)) + ") TOCKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                            + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));
                    naloga.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                    naloga.add(1,temp[i]);
                    listNaloge.add(naloga);
                    i++;

                }

            } else{

                if (!StringUtils.isEmpty(datumDo)){
                    if ((!java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(datumDo))) &&
                            java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(NAZIV_NALOGA))
                                + " (" + res.getString(res.getColumnIndex(DATUM_ODDAJE)) + ") TOCKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                                + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));
                        naloga.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                        naloga.add(1,temp[i]);
                        listNaloge.add(naloga);
                        i++;

                    }

                } else {
                    if (java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(NAZIV_NALOGA))
                                + " (" + res.getString(res.getColumnIndex(DATUM_ODDAJE)) + ") TOCKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                                + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));
                        naloga.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                        naloga.add(1,temp[i]);
                        listNaloge.add(naloga);
                        i++;
                    }

                }

            }



            res.moveToNext();
        }
        /**
         *  Prepisovanje v drugo polje
         */

        String [] naloge = new String[i];
        for (int z = 0; z < i; z++){
            naloge[z] = temp[z];

            //System.out.println("as"+naloge[z]);
        }


        for(ArrayList<Object> list: listNaloge){
            System.out.println(list.get(0)+", "+list.get(1));
        }

        //return naloge;
        return listNaloge;
    }

    /**
     *  GET IZPITI
     **/

    public ArrayList<ArrayList<Object>>  getAllIzpiti(boolean nalogeZaNazaj, String datumDo) {

        String[] temp = new String[numberOfRows(IZPIT_TABLE_NAME)];
        int i = 0;
        ArrayList<ArrayList<Object>> listIzpiti=new ArrayList<ArrayList<Object>>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = null;

        Date currentDate = new Date();
        String danasnjiDatum = formatterDate.format(currentDate);

       /* res =  db.rawQuery("SELECT " + NAZIV_PREDMET + ", " + ST_IZPITA + ", " + ST_MOZNIH_TOCK_IZPIT + ", " + GRADIVO + ", " + DATUM_IZPITA + ", " + TIP + " FROM " + PREDMET_TABLE_NAME +
                    " INNER JOIN " + IZPIT_TABLE_NAME + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDPREDMET +
                    " INNER JOIN " + TIP_IZPITA_TABLE_NAME + " ON " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDTIP_IZPITA + " = " + TIP_IZPITA_TABLE_NAME + "." + ID_TIP_IZPITA + " ORDER BY date(" + DATUM_IZPITA + ") ASC;" , null );

        */

        res =  db.rawQuery("SELECT " + NAZIV_PREDMET + ", " + ST_IZPITA + ", " + ST_MOZNIH_TOCK_IZPIT + ", " + ST_TRENUTNIH_TOCK_IZPIT + ", " + GRADIVO + ", " + DATUM_IZPITA + ", " + TIP + " FROM " + PREDMET_TABLE_NAME +
                " INNER JOIN " + IZPIT_TABLE_NAME + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDPREDMET +
                " INNER JOIN " + TIP_IZPITA_TABLE_NAME + " ON " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDTIP_IZPITA + " = " + TIP_IZPITA_TABLE_NAME + "." + ID_TIP_IZPITA + " ORDER BY date(" + DATUM_IZPITA + ") ASC;" , null );



        res.moveToFirst();
        while(res.isAfterLast() == false){
            ArrayList<Object> izpit=new ArrayList<Object>();
            if (nalogeZaNazaj){

                if (!StringUtils.isEmpty(datumDo)){

                    if (!java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(datumDo))) {

                        temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(TIP))+ " "
                                + res.getString(res.getColumnIndex(ST_IZPITA)) + " "
                                + " (" + res.getString(res.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                                + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));

                        izpit.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                        izpit.add(1,temp[i]);
                        listIzpiti.add(izpit);
                        i++;

                    }

                }else {

                    temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(TIP))+ " "
                            + res.getString(res.getColumnIndex(ST_IZPITA)) + " "
                            + " (" + res.getString(res.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                            + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                    izpit.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                    izpit.add(1,temp[i]);
                    listIzpiti.add(izpit);
                    i++;

                }

            } else{

                if (!StringUtils.isEmpty(datumDo)){
                    if ((!java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(datumDo))) &&
                            java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(TIP))+ " "
                                + res.getString(res.getColumnIndex(ST_IZPITA)) + " "
                                + " (" + res.getString(res.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                                + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                        izpit.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                        izpit.add(1,temp[i]);
                        listIzpiti.add(izpit);
                        i++;

                    }

                } else {
                    if (java.sql.Date.valueOf(res.getString(res.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        temp[i] = "[" + res.getString(res.getColumnIndex(NAZIV_PREDMET)) + "] " + res.getString(res.getColumnIndex(TIP))+ " "
                                + res.getString(res.getColumnIndex(ST_IZPITA)) + " "
                                + " (" + res.getString(res.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                                + res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                        izpit.add(0,res.getString(res.getColumnIndex(NAZIV_PREDMET)));
                        izpit.add(1,temp[i]);
                        listIzpiti.add(izpit);
                        i++;
                    }

                }

            }

            res.moveToNext();
        }

        /**
         *  Prepisovanje v drugo polje
         */

        String [] izpiti = new String[i];
        for (int z = 0; z < i; z++){
            izpiti[z] = temp[z];
            //System.out.println("as"+izpiti[z]);
        }

        return listIzpiti;
    }

    /**
     *  GET IZPITI
     **/

    public String[] getAllIzpitiInNaloge(boolean zaNazaj, String datumDo) {

        String[] tempNaloge = new String[numberOfRows(NALOGA_TABLE_NAME)];
        String[] tempIzpiti = new String[numberOfRows(IZPIT_TABLE_NAME)];

        int i = 0, n = 0;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor resNaloge = null, resIzpiti = null;

        Date currentDate = new Date();
        String danasnjiDatum = formatterDate.format(currentDate);

        resNaloge =  db.rawQuery("SELECT " + NAZIV_PREDMET + ", " + OPIS_PREDMET + ", " + PREDAVATELJ + ", " + ASISTENT + "," + MOZNE_TOCKE_NALOGE + ", " + MOZNE_TOCKE_IZPITI + ", "
                    + NAZIV_NALOGA + "," + DATUM_ODDAJE + ", " + DATUM_OBJAVE + ", " + URA_ODDAJE + ", " + OPIS_NALOGA + ", " + ST_MOZNIH_TOCK_NALOGA + "," + ST_TRENUTNIH_TOCK_NALOGA + "," + FK_NALOGA_IDPREDMET + " "
                    +"  FROM " + PREDMET_TABLE_NAME + " INNER JOIN " + NALOGA_TABLE_NAME
                    + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + NALOGA_TABLE_NAME + "." + FK_NALOGA_IDPREDMET + " ORDER BY date(" + DATUM_ODDAJE + ") ASC;" , null );


        resNaloge.moveToFirst();
        while(resNaloge.isAfterLast() == false) {
            if (zaNazaj) {

                if (!StringUtils.isEmpty(datumDo)) {

                    if (!(java.sql.Date.valueOf(resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(datumDo)))){
                        System.out.println("DATUM DO _----" + datumDo);
                        tempNaloge[n] = "[" + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_PREDMET)) + "] " + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_NALOGA))
                                + " (" + resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE)) + ") TOČKE: " + resNaloge.getString(resNaloge.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                                + resNaloge.getString(resNaloge.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));

                        n++;

                    }

                } else {

                    tempNaloge[n] = "[" + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_PREDMET)) + "] " + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_NALOGA))
                            + " (" + resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE)) + ") TOČKE: " + resNaloge.getString(resNaloge.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                            + resNaloge.getString(resNaloge.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));

                    n++;

                }

            } else {

                if (!StringUtils.isEmpty(datumDo)) {
                    if ((!java.sql.Date.valueOf(resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(datumDo))) &&
                            java.sql.Date.valueOf(resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        tempNaloge[n] = "[" + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_PREDMET)) + "] " + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_NALOGA))
                                + " (" + resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE)) + ") TOČKE: " + resNaloge.getString(resNaloge.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                                + resNaloge.getString(resNaloge.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));

                        n++;

                    }

                } else {
                    if (java.sql.Date.valueOf(resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        tempNaloge[n] = "[" + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_PREDMET)) + "] " + resNaloge.getString(resNaloge.getColumnIndex(NAZIV_NALOGA))
                                + " (" + resNaloge.getString(resNaloge.getColumnIndex(DATUM_ODDAJE)) + ") TOČKE: " + resNaloge.getString(resNaloge.getColumnIndex(ST_MOZNIH_TOCK_NALOGA)) + "/"
                                + resNaloge.getString(resNaloge.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA));

                        n++;
                    }

                }

            }


            resNaloge.moveToNext();
        }




        resIzpiti =  db.rawQuery("SELECT " + NAZIV_PREDMET + ", " + ST_IZPITA + ", " + ST_MOZNIH_TOCK_IZPIT + ", " + ST_TRENUTNIH_TOCK_IZPIT + ", " + GRADIVO + ", " + DATUM_IZPITA + ", " + TIP + " FROM " + PREDMET_TABLE_NAME +
                " INNER JOIN " + IZPIT_TABLE_NAME + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDPREDMET +
                " INNER JOIN " + TIP_IZPITA_TABLE_NAME + " ON " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDTIP_IZPITA + " = " + TIP_IZPITA_TABLE_NAME + "." + ID_TIP_IZPITA + " ORDER BY date(" + DATUM_IZPITA + ") ASC;" , null );

        resIzpiti.moveToFirst();
        while(resIzpiti.isAfterLast() == false){
            if (zaNazaj){

                if (!StringUtils.isEmpty(datumDo)){

                    if (!java.sql.Date.valueOf(resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(datumDo))) {

                        tempIzpiti[i] = "[" + resIzpiti.getString(resIzpiti.getColumnIndex(NAZIV_PREDMET)) + "] " + resIzpiti.getString(resIzpiti.getColumnIndex(TIP))+ " "
                                + resIzpiti.getString(resIzpiti.getColumnIndex(ST_IZPITA)) + " "
                                + " (" + resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + resIzpiti.getString(resIzpiti.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                                + resIzpiti.getString(resIzpiti.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                        i++;

                    }

                }else {

                    tempIzpiti[i] = "[" + resIzpiti.getString(resIzpiti.getColumnIndex(NAZIV_PREDMET)) + "] " + resIzpiti.getString(resIzpiti.getColumnIndex(TIP))+ " "
                            + resIzpiti.getString(resIzpiti.getColumnIndex(ST_IZPITA)) + " "
                            + " (" + resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + resIzpiti.getString(resIzpiti.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                            + resIzpiti.getString(resIzpiti.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                    i++;

                }

            } else{

                if (!StringUtils.isEmpty(datumDo)){
                    if ((!java.sql.Date.valueOf(resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(datumDo))) &&
                            java.sql.Date.valueOf(resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        tempIzpiti[i] = "[" + resIzpiti.getString(resIzpiti.getColumnIndex(NAZIV_PREDMET)) + "] " + resIzpiti.getString(resIzpiti.getColumnIndex(TIP))+ " "
                                + resIzpiti.getString(resIzpiti.getColumnIndex(ST_IZPITA)) + " "
                                + " (" + resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + resIzpiti.getString(resIzpiti.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                                + resIzpiti.getString(resIzpiti.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                        i++;

                    }

                } else {
                    if (java.sql.Date.valueOf(resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA))).after(java.sql.Date.valueOf(danasnjiDatum))) {

                        tempIzpiti[i] = "[" + resIzpiti.getString(resIzpiti.getColumnIndex(NAZIV_PREDMET)) + "] " + resIzpiti.getString(resIzpiti.getColumnIndex(TIP)) + " "
                                + resIzpiti.getString(resIzpiti.getColumnIndex(ST_IZPITA)) + " "
                                + " (" + resIzpiti.getString(resIzpiti.getColumnIndex(DATUM_IZPITA)) + ") TOČKE: " + resIzpiti.getString(resIzpiti.getColumnIndex(ST_MOZNIH_TOCK_IZPIT)) + "/"
                                + resIzpiti.getString(resIzpiti.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT));
                        i++;
                    }

                }

            }

            resIzpiti.moveToNext();
        }

        String [] izpitiInNaloge = new String[n+i];
        for (int z = 0; z < n; z++){
            izpitiInNaloge[z] = tempNaloge[z];
        }
        int f = 0;
        for (int z = n; z < i+n; z++){
            izpitiInNaloge[z] = tempIzpiti[f];
            f++;
        }

        /**
         * rasporejanje po datumu
         */

        String [] sortirano = new String[izpitiInNaloge.length];
        for(int j = 0; j < izpitiInNaloge.length ; j++){

        }


        return izpitiInNaloge;
    }


    /**
     * GET NALOGA
     * @param nazivPredmeta
     * @param datumOddaje
     * @param nazivNaloge
     * @return
     */

     public Naloga getNaloga(String nazivPredmeta, String datumOddaje, String nazivNaloge) {

         SQLiteDatabase db = this.getReadableDatabase();
         Naloga naloga = new Naloga();
         Cursor res =  db.rawQuery( "SELECT " + NAZIV_PREDMET + ", " + MOZNE_TOCKE_NALOGE + ", " + MOZNE_TOCKE_IZPITI + ", "
                 + ID_NALOGA + "," + NAZIV_NALOGA + "," + DATUM_ODDAJE + ", " + DATUM_OBJAVE + ", " + URA_ODDAJE + ", " + OPIS_NALOGA + ", " + ST_MOZNIH_TOCK_NALOGA + ", " + ST_TRENUTNIH_TOCK_NALOGA + "," + FK_NALOGA_IDPREDMET + " "
                 +"  FROM " + PREDMET_TABLE_NAME + " INNER JOIN " + NALOGA_TABLE_NAME
                 + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + NALOGA_TABLE_NAME + "." + FK_NALOGA_IDPREDMET + ""
                 + " WHERE " + NAZIV_PREDMET + " = '" + nazivPredmeta + "' AND " + NAZIV_NALOGA
                 + " = '" + nazivNaloge + "' LIMIT 1;", null );

         if (res.moveToFirst()) {

             naloga.setId(Integer.parseInt(res.getString(res.getColumnIndex(ID_NALOGA))));
             naloga.setNaziv(res.getString(res.getColumnIndex(NAZIV_NALOGA)));
             naloga.setOpis(res.getString(res.getColumnIndex(OPIS_NALOGA)));

             try {
                 naloga.setDatumOddaje(new java.sql.Date(formatterDate.parse(res.getString(res.getColumnIndex(DATUM_ODDAJE))).getTime()));
                 naloga.setDatumObjave(new java.sql.Date(formatterDate.parse(res.getString(res.getColumnIndex(DATUM_OBJAVE))).getTime()));
                 naloga.setUraOddaje(new java.sql.Time(formatterTime.parse(res.getString(res.getColumnIndex(URA_ODDAJE))).getTime()));
             } catch (ParseException e) {
                 e.printStackTrace();
             }

             naloga.setStMoznihTock(Integer.parseInt(res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_NALOGA))));
             naloga.setStTrenutnihTock(Integer.parseInt(res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_NALOGA))));

         }

         return naloga;
     }

    /**
     * GET IZPIT
     * @param stIzpita
     * @param nazivPredmeta
     * @param tipIzpita
     * @param datumIzpita
     * @return
     */
    public Izpit getIzpit(int stIzpita, String nazivPredmeta, String tipIzpita, String datumIzpita) {

        SQLiteDatabase db = this.getReadableDatabase();
        Izpit izpit = new Izpit();

        Cursor res =  db.rawQuery("SELECT " + NAZIV_PREDMET + ", " + ID_IZPIT + ", " + ST_IZPITA + ", " + ST_MOZNIH_TOCK_IZPIT + ", " + GRADIVO + ", " + DATUM_IZPITA + ", " + TIP + ", " + ST_TRENUTNIH_TOCK_IZPIT + " FROM " + PREDMET_TABLE_NAME +
                " INNER JOIN " + IZPIT_TABLE_NAME + " ON " + PREDMET_TABLE_NAME + "." + ID_PREDMET + " = " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDPREDMET +
                " INNER JOIN " + TIP_IZPITA_TABLE_NAME + " ON " + IZPIT_TABLE_NAME + "." + FK_IZPIT_IDTIP_IZPITA + " = " + TIP_IZPITA_TABLE_NAME + "." + ID_TIP_IZPITA
                +" WHERE "+ NAZIV_PREDMET + " = '" + nazivPredmeta + "' AND " + TIP + " = '" + tipIzpita + "' AND " + ST_IZPITA + " = " + stIzpita + " LIMIT 1;", null );

        if (res.moveToFirst()) {

            izpit.setId(Integer.parseInt(res.getString(res.getColumnIndex(ID_IZPIT))));
            izpit.setSt(Integer.parseInt(res.getString(res.getColumnIndex(ST_IZPITA))));
            izpit.setTip(res.getString(res.getColumnIndex(TIP)));
            izpit.setStMoznihTock(Integer.parseInt(res.getString(res.getColumnIndex(ST_MOZNIH_TOCK_IZPIT))));
            izpit.setStTrenutnihTock(Integer.parseInt(res.getString(res.getColumnIndex(ST_TRENUTNIH_TOCK_IZPIT))));
            izpit.setGradivo(res.getString(res.getColumnIndex(GRADIVO)));
            try {
                izpit.setDatumIzpita(new java.sql.Date(formatterDate.parse(res.getString(res.getColumnIndex(DATUM_IZPITA))).getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return izpit;
    }

     public int numberOfRows(String tablename) {
         SQLiteDatabase db = this.getReadableDatabase();
         int numRows = (int) DatabaseUtils.queryNumEntries(db, tablename);
         return numRows;
     }


}
