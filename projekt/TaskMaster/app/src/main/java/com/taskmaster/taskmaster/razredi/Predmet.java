package com.taskmaster.taskmaster.razredi;
import java.util.ArrayList;

/**
 * Created by Anastazia on 22. 05. 2017.
 */

public class Predmet {

    private String naziv;
    private String opis;
    private String predavatelj;
    private String asistent;
    private String obdobjeIzvajanja;
    private int mozneTockeNaloge;
    private int mozneTockeIzpiti;
    private ArrayList<Naloga> naloge;
    private ArrayList<Izpit> izpiti;

    public Predmet() {
    }

    public Predmet(String naziv, String opis, String predavatelj, String asistent, String obdobjeIzvajanja, int mozneTockeNaloge, int mozneTockeIzpiti) {
        this.naziv = naziv;
        this.opis = opis;
        this.predavatelj = predavatelj;
        this.asistent = asistent;
        this.obdobjeIzvajanja = obdobjeIzvajanja;
        this.mozneTockeNaloge = mozneTockeNaloge;
        this.mozneTockeIzpiti = mozneTockeIzpiti;
    }

    public Predmet(String naziv, String opis, String predavatelj, String asistent, String obdobjeIzvajanja, int mozneTockeNaloge, int mozneTockeIzpiti, ArrayList<Naloga> naloge) {
        this.naziv = naziv;
        this.opis = opis;
        this.predavatelj = predavatelj;
        this.asistent = asistent;
        this.obdobjeIzvajanja = obdobjeIzvajanja;
        this.mozneTockeNaloge = mozneTockeNaloge;
        this.mozneTockeIzpiti = mozneTockeIzpiti;
        this.naloge = naloge;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getPredavatelj() {
        return predavatelj;
    }

    public void setPredavatelj(String predavatelj) {
        this.predavatelj = predavatelj;
    }

    public String getAsistent() {
        return asistent;
    }

    public void setAsistent(String asistent) {
        this.asistent = asistent;
    }

    public String getObdobjeIzvajanja() {
        return obdobjeIzvajanja;
    }

    public void setObdobjeIzvajanja(String obdobjeIzvajanja) {
        this.obdobjeIzvajanja = obdobjeIzvajanja;
    }

    public int getMozneTockeNaloge() {
        return mozneTockeNaloge;
    }

    public void setMozneTockeNaloge(int mozneTockeNaloge) {
        this.mozneTockeNaloge = mozneTockeNaloge;
    }

    public int getMozneTockeIzpiti() {
        return mozneTockeIzpiti;
    }

    public void setMozneTockeIzpiti(int mozneTockeIzpiti) {
        this.mozneTockeIzpiti = mozneTockeIzpiti;
    }

    public ArrayList<Naloga> getNaloge() {
        return naloge;
    }

    public void setNaloge(ArrayList<Naloga> naloge) {
        this.naloge = naloge;
    }

    public ArrayList<Izpit> getIzpiti() {
        return izpiti;
    }

    public void setIzpiti(ArrayList<Izpit> izpiti) {
        this.izpiti = izpiti;
    }
}
