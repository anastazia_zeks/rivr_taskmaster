package com.taskmaster.taskmaster.razredi;

/**
 * Created by Anastazia on 1. 06. 2017.
 */

public class Uporabnik {

    private int id;
    private String ime;
    private String priimek;
    private String vzdevek;
    private String email;
    private int ikona;

    public Uporabnik() {
    }

    public Uporabnik(int id, String ime, String priimek, String vzdevek, String email,int ikona) {
        this.id = id;
        this.ime = ime;
        this.priimek = priimek;
        this.vzdevek = vzdevek;
        this.email = email;
        this.ikona=ikona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getVzdevek() {
        return vzdevek;
    }

    public void setVzdevek(String vzdevek) {
        this.vzdevek = vzdevek;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIkona() {
        return ikona;
    }

    public void setIkona(int ikona) {
        this.ikona = ikona;
    }

    @Override
    public String toString() {
        return "Uporabnik{" +
                "id=" + id +
                ", ime='" + ime + '\'' +
                ", priimek='" + priimek + '\'' +
                ", vzdevek='" + vzdevek + '\'' +
                ", email='" + email + '\'' +
                ", ikona=" + ikona +
                '}';
    }
}
