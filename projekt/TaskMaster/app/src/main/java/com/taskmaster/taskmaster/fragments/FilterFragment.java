package com.taskmaster.taskmaster.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.taskmaster.taskmaster.PregledNalog;
import com.taskmaster.taskmaster.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FilterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static int FILTER_PRIKAZ_NALOGE_IZPITI=0; //prikazi vse
    public static int FILTER_PO_DATUMU=0;
    public static boolean FILTER_NALOGE_ZA_NAZAJ=false;

    private Spinner prikaz_nalog_spinner;
    private CheckBox prikazi_naloge_nazaj_checkbox;
    private Spinner po_datumu_spinner;
    private Button button_shrani;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterFragment newInstance(String param1, String param2) {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View myFragmentView =inflater.inflate(R.layout.fragment_filter, container, false);

        prikaz_nalog_spinner= (Spinner) myFragmentView.findViewById(R.id.prikaz_nalog_id);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.prikaz_nalog_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prikaz_nalog_spinner.setAdapter(adapter);
        prikaz_nalog_spinner.setSelection(FILTER_PRIKAZ_NALOGE_IZPITI);

        prikaz_nalog_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FILTER_PRIKAZ_NALOGE_IZPITI=prikaz_nalog_spinner.getSelectedItemPosition();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                FILTER_PRIKAZ_NALOGE_IZPITI=0;
            }
        });



        po_datumu_spinner= (Spinner) myFragmentView.findViewById(R.id.po_datumu_id);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(), R.array.po_datumu_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        po_datumu_spinner.setAdapter(adapter2);
        po_datumu_spinner.setSelection(FILTER_PO_DATUMU);

        po_datumu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FILTER_PO_DATUMU=po_datumu_spinner.getSelectedItemPosition();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                FILTER_PO_DATUMU=0;
            }
        });



        prikazi_naloge_nazaj_checkbox=(CheckBox) myFragmentView.findViewById(R.id.prikazi_naloge_nazaj_id);
        prikazi_naloge_nazaj_checkbox.setChecked(FILTER_NALOGE_ZA_NAZAJ);
        prikazi_naloge_nazaj_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FILTER_NALOGE_ZA_NAZAJ=isChecked;
            }
        });


        button_shrani=(Button) myFragmentView.findViewById(R.id.filter_shrani_button);
        button_shrani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(getActivity().getApplicationContext(), PregledNalog.class);

                mainIntent.putExtra("FILTER_NALOGE_ZA_NAZAJ", FILTER_NALOGE_ZA_NAZAJ);
                mainIntent.putExtra("FILTER_PRIKAZ_NALOGE_IZPITI", FILTER_PRIKAZ_NALOGE_IZPITI); // 0 JE VSE
                mainIntent.putExtra("FILTER_PO_DATUMU", FILTER_PO_DATUMU); // prikaz v tednih

                startActivity(mainIntent);
            }
        });


        return myFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

 /*   @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
