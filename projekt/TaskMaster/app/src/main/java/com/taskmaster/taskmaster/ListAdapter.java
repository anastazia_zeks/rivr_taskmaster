package com.taskmaster.taskmaster;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jerne on 10. 06. 2017.
 */

public class ListAdapter extends ArrayAdapter<ArrayList<Object>> {

    private Context mContext;
    private int id;
    private List<ArrayList<Object>> items ;

    public ListAdapter(Context context, int textViewResourceId , List<ArrayList<Object>> list )
    {
        super(context, textViewResourceId, list);
        mContext = context;
        id = textViewResourceId;
        items = list ;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        View mView = v ;
        if(mView == null){
            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = vi.inflate(id, null);
        }

        TextView text = (TextView) mView.findViewById(R.id.textView);


        if((int)items.get(position).get(0)==0){ //naloge
            text.setText(items.get(position).get(1).toString());
            text.setTextColor(ContextCompat.getColor(getContext(),R.color.text_gray));
            text.setBackgroundColor(fetchcolor((int)items.get(position).get(3)));
        }
        else if((int)items.get(position).get(0)==1){
            text.setText(items.get(position).get(1).toString());
            text.setTextColor(ContextCompat.getColor(getContext(),R.color.text_gray));
            text.setBackgroundColor(fetchcolor((int)items.get(position).get(3)));
        }


        return mView;
    }

    private int fetchcolor(int color) {
       // TypedValue typedValue = new TypedValue();

       // TypedArray a = mContext.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimaryDark });
       // int color = a.getColor(0, 0);
       // a.recycle();

        int halfTransparentColor = adjustAlpha(color, 0.3f);

        return halfTransparentColor;
    }


    private int fetchcolorPrimary() {
        TypedValue typedValue = new TypedValue();

        TypedArray a = mContext.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimary });
        int color = a.getColor(0, 0);
        a.recycle();

        int halfTransparentColor = adjustAlpha(color, 0.2f);

        return halfTransparentColor;
    }

    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

}