package com.taskmaster.taskmaster.parser;

/**
 * Created by Anastazia on 22. 05. 2017.
 */

public class UrlManager {

    private String baseUrl;
    private String fakulteta;
    private String zvrst;
    private String smer;
    private String letnik;
    private String semester;
    private String fileName;

    public UrlManager() {
    }

    public UrlManager(String fakulteta, String zvrst, String smer, String letnik, String semester) {
        this.baseUrl = "https://taskmaster.000webhostapp.com/";
        this.fakulteta = fakulteta;
        this.zvrst = zvrst;
        this.smer = smer;
        this.letnik = letnik;
        this.semester = semester;
        this.fileName = "struktura.json";
    }

    @Override
    public String toString() {
        return baseUrl + fakulteta + "/" + zvrst + "/" + smer + "/" + letnik + "/" + semester + "/" + fileName;
    }
}
