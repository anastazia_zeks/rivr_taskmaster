package com.taskmaster.taskmaster;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.taskmaster.taskmaster.dodatki.DBHelper;
import com.taskmaster.taskmaster.razredi.Izpit;
import com.taskmaster.taskmaster.razredi.Naloga;

import org.apache.commons.lang3.StringUtils;

public class Pregled extends AppCompatActivity {

    private TextView naslovTV, opisTV, stMoznihTockTV, stTrenutnihTock, datumTV;
    private Button koncaj;
    private DBHelper dbh;
    private Naloga naloga;
    private Izpit izpit;
    private double maxTocke = 0;
    private boolean isIzpit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregled);

        naslovTV = (TextView) findViewById(R.id.naslov_text);
        opisTV = (TextView) findViewById(R.id.opis_text);
        stMoznihTockTV = (TextView) findViewById(R.id.st_moznih_tock_text);
        stTrenutnihTock = (TextView) findViewById(R.id.st_trenutnih_tock_text);
        datumTV = (TextView) findViewById(R.id.datum_text);

        dbh = new DBHelper(getApplicationContext());
        String nazivPredmeta = null, naziv = null, datum = null;

        int stIzpita = 0;
        String tipIzpita = null;

        Bundle pregledNalog = getIntent().getExtras();

        if(pregledNalog != null){
            String opis = pregledNalog.getString("opis");

            nazivPredmeta = getValuesInsideParenthesis(opis, 1);
            naziv = getValuesInsideParenthesis(opis, 2);
            datum = getValuesInsideParenthesis(opis, 3);


        }

        nazivPredmeta = nazivPredmeta.trim();
        datum = datum.trim();
        naziv = naziv.trim();

        if(!StringUtils.isEmpty(naziv) && (naziv.contains("IZPIT") || naziv.contains("KOLOKVIJ"))) {
            isIzpit = true;
            if (naziv.contains("IZPIT")){

                tipIzpita = "IZPIT";
                if (naziv.contains("1")) {
                    stIzpita = 1;

                } else if (naziv.contains("2")) {
                    stIzpita = 2;

                } else if (naziv.contains("3")) {
                    stIzpita = 3;

                } else if (naziv.contains("3")) {
                    stIzpita = 4;
                }

                izpit = dbh.getIzpit(stIzpita, nazivPredmeta, tipIzpita, datum);

            } else  if (naziv.contains("KOLOKVIJ")) {

                tipIzpita = "KOLOKVIJ";
                if (naziv.contains("1")) {
                    stIzpita = 1;

                } else if (naziv.contains("2")) {
                    stIzpita = 2;

                } else if (naziv.contains("3")) {
                    stIzpita = 3;

                }

                izpit = dbh.getIzpit(stIzpita, nazivPredmeta, tipIzpita, datum);
            }

        } else  if (!StringUtils.isEmpty(naziv)){
            isIzpit = false;
            naloga = dbh.getNaloga(nazivPredmeta, datum, naziv);
        }

        if (naloga != null){

            naslovTV.setText("[" +nazivPredmeta + "] " + naloga.getNaziv());
            opisTV.setText("OPIS NALOGE: " +naloga.getOpis());
            stMoznihTockTV.setText("ŠTEVILO PIK KI IH LAHKO DOSEŽETE: " + Double.toString(naloga.getStMoznihTock()));
            stTrenutnihTock.setText("ŠTEVILO PIK KI IH IMATE: " + Double.toString(naloga.getStTrenutnihTock()));
            maxTocke = naloga.getStMoznihTock();
            datumTV.setText("DATUM ODDAJE: " + naloga.getDatumOddaje().toString());

        } else if(izpit != null){

            naslovTV.setText("[" +nazivPredmeta + "] " + izpit.getTip() + " " + izpit.getSt());
            opisTV.setText("GRADIVO: " + izpit.getGradivo());
            stMoznihTockTV.setText("ŠTEVILO TOČK KI IH LAHKO DOSEŽETE: " + Double.toString(izpit.getStMoznihTock()));
            stTrenutnihTock.setText("ŠTEVILO TOČK KI STE IH DOSEGLI: " + Double.toString(izpit.getStTrenutnihTock()));
            maxTocke = izpit.getStMoznihTock();
            datumTV.setText("DATUM IZPITA: " + izpit.getDatumIzpita());

        }

        koncaj = (Button)  findViewById(R.id.koncaj_button);
        koncaj.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                final AlertDialog.Builder koncajDialog = new AlertDialog.Builder(Pregled.this);
                View adview = getLayoutInflater().inflate(R.layout.vnesi_pike, null);
                koncajDialog.setView(adview);
                AlertDialog dialog = koncajDialog.create();

                final EditText stPridobljenihPik = (EditText) adview.findViewById(R.id.st_pik_input);

                Button vnosPikBtn = (Button)  adview.findViewById(R.id.vnos_pik_btn);

                vnosPikBtn.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {

                        if(Double.parseDouble(stPridobljenihPik.getText().toString()) <= maxTocke){

                            if(isIzpit){
                                boolean uspesnoVneseno = dbh.updateIzpit(izpit.getId(), Double.parseDouble(stPridobljenihPik.getText().toString()));
                                // update predmet
                                if(uspesnoVneseno){
                                    int idPredmet = dbh.getPredmetID(izpit.getId(), "izpit");
                                    double trenutneTockeIzpitov = dbh.getTrenutneTockeForPredmet(idPredmet, "izpit");
                                    dbh.updatePredmet(idPredmet, trenutneTockeIzpitov, "izpit");

                                    System.out.println("TRENUTNE TOCKE: izpitov " + trenutneTockeIzpitov);
                                    // update tocke
                                    double stMoznihTock = dbh.getTockeForTocke(Integer.parseInt(dbh.getLastID("uporabnik")), "mozne");
                                    double stTrenutnihTock =  dbh.getTockeForTocke(Integer.parseInt(dbh.getLastID("uporabnik")), "trenutne");
                                    int levelID = Integer.parseInt(dbh.getLevelIDByTocke(stTrenutnihTock));
                                    dbh.insertTocke(stMoznihTock, stTrenutnihTock, Integer.parseInt(dbh.getLastID("uporabnik")), levelID);

                                    System.out.println("TRENUTNE TOCKE: " + stTrenutnihTock + " mozne tocke  "+ stMoznihTock);
                                    Toast.makeText(getBaseContext(), "Vnos točk je uspešen!", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getBaseContext(), "Nekaj je šlo narobe!", Toast.LENGTH_LONG).show();
                                }

                            }else {
                                boolean uspesnoVneseno = dbh.updateNaloga(naloga.getId(), Double.parseDouble(stPridobljenihPik.getText().toString()));
                                if(uspesnoVneseno){
                                    //update predmet
                                    int idPredmet = dbh.getPredmetID(naloga.getId(), "naloga");
                                    double trenutneTockeNalog = dbh.getTrenutneTockeForPredmet(idPredmet, "naloga");
                                    dbh.updatePredmet(idPredmet, trenutneTockeNalog, "naloga");
                                    System.out.println("TRENUTNE TOCKE: nalog " + trenutneTockeNalog);
                                    //update tocke

                                    double stMoznihTock = dbh.getTockeForTocke(Integer.parseInt(dbh.getLastID("uporabnik")), "mozne");
                                    double stTrenutnihTock =  dbh.getTockeForTocke(Integer.parseInt(dbh.getLastID("uporabnik")), "trenutne");
                                    int levelID = Integer.parseInt(dbh.getLevelIDByTocke(stTrenutnihTock));
                                    dbh.insertTocke(stMoznihTock, stTrenutnihTock, Integer.parseInt(dbh.getLastID("uporabnik")), levelID);

                                    System.out.println("TRENUTNE TOCKE: " + stMoznihTock + " mozne tocke  "+ stMoznihTock);

                                    Toast.makeText(getBaseContext(), "Vnos točk je uspešen!", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getBaseContext(), "Nekaj je šlo narobe!", Toast.LENGTH_LONG).show();
                                }
                            }


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent mainIntent = new Intent(Pregled.this, PregledNalog.class);
                                    startActivity(mainIntent);
                                }
                            }, 500);



                        } else {
                            Toast.makeText(getBaseContext(), "Število vnešenih točk ni v skladu z maksimalnim številom točk!", Toast.LENGTH_LONG).show();
                        }


                    }

                });

                dialog.show();
            }

        });
    }

    public String getValuesInsideParenthesis(String value, int type){

        String temp = null;

        switch (type){

            case 1:
                // znotraj [] oklepajih
                temp = value.substring(value.indexOf('[')+1, value.indexOf(']'));
                break;
            case 2:
                // izven vsakih oklepajih pri nalogaj in izpitaj -> opomba: ne smejo se uporabljati () oklepaji pri nazivu
                temp = value.substring(value.indexOf(']')+1, value.indexOf('(')-1);
                break;

            case 3:
                // znotraj () oklepajih
                temp = value.substring(value.indexOf('(')+1, value.indexOf(')'));
                break;

            default:
                System.out.println("Napačna vrednost je vstavljena!");
                break;
        }
        //System.out.println(" Sem v metodi substring  " + temp);

        return temp;
    }

}
