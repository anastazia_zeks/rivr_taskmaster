package com.taskmaster.taskmaster.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.taskmaster.taskmaster.ListAdapter;
import com.taskmaster.taskmaster.Pregled;
import com.taskmaster.taskmaster.R;
import com.taskmaster.taskmaster.dodatki.DBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PregledNalogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PregledNalogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PregledNalogFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private boolean FILTER_NALOGE_ZA_NAZAJ = true;
    private int FILTER_PRIKAZ_NALOGE_IZPITI = 0;
    private int FILTER_PO_DATUMU = 0;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private DBHelper dbh;


    private OnFragmentInteractionListener mListener;

    public PregledNalogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PregledNalogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PregledNalogFragment newInstance(String param1, String param2) {
        PregledNalogFragment fragment = new PregledNalogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getActivity().getIntent().getExtras() != null){
            FILTER_NALOGE_ZA_NAZAJ  = getActivity().getIntent().getExtras().getBoolean("FILTER_NALOGE_ZA_NAZAJ", false);
            FILTER_PRIKAZ_NALOGE_IZPITI  =  getActivity().getIntent().getExtras().getInt("FILTER_PRIKAZ_NALOGE_IZPITI", 0);
            FILTER_PO_DATUMU  =  getActivity().getIntent().getExtras().getInt("FILTER_PO_DATUMU", 0);

        }

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView= inflater.inflate(R.layout.fragment_pregled_nalog, container, false);

        dbh = new DBHelper(getActivity().getApplicationContext());

        ArrayList<ArrayList<Object>> arrayIzpisPregledaNalog = new ArrayList<ArrayList<Object>>();
        ArrayList<ArrayList<Object>> arrayIzpisPregledaIzpitov = new ArrayList<ArrayList<Object>>();
        //DATUM ZA NALOGE
        //Date utilDate = new Date();
        //java.text.DateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
        //java.sql.Date datumDo = new java.sql.Date(utilDate.getTime());

        String datumDo = null;

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = (Calendar.getInstance().getTime());
        cal.setTime(curDate);


            switch (FILTER_PO_DATUMU){
            // 2 tedna naprej
                case 0:
                    cal.add(Calendar.DATE, 14);
                    curDate = cal.getTime();
                    datumDo = formatterDate.format(curDate);
                    System.out.println("datum do case 0>>" + datumDo);

                    break;
            // 3 tedna naprej
                case 1:
                    cal.add(Calendar.DATE, 21);
                    curDate = cal.getTime();
                    datumDo = formatterDate.format(curDate);
                    System.out.println("datum do case 1>>" + datumDo);

                    break;

                default:
                    cal.add(Calendar.DATE, 21);
                    curDate = cal.getTime();
                    datumDo = formatterDate.format(curDate);
                    break;
            }
        switch (FILTER_PRIKAZ_NALOGE_IZPITI){
            // izpis vseh nalog in izpitov
            case 0:
                arrayIzpisPregledaNalog = dbh.getAllNaloge(FILTER_NALOGE_ZA_NAZAJ, datumDo);
                arrayIzpisPregledaIzpitov = dbh.getAllIzpiti(FILTER_NALOGE_ZA_NAZAJ, datumDo);
                break;

            // samo naloge izpiši
            case 1:
                arrayIzpisPregledaNalog = dbh.getAllNaloge(FILTER_NALOGE_ZA_NAZAJ, datumDo);
                break;

            // samo izpite
            case 2:
                arrayIzpisPregledaIzpitov = dbh.getAllIzpiti(FILTER_NALOGE_ZA_NAZAJ, datumDo);
                break;

            // izpis vseh nalog
            default:
                arrayIzpisPregledaNalog = dbh.getAllNaloge(FILTER_NALOGE_ZA_NAZAJ, datumDo);
                arrayIzpisPregledaIzpitov = dbh.getAllIzpiti(FILTER_NALOGE_ZA_NAZAJ, datumDo);
                break;
        }
        System.out.println("datum do case 000>>" + datumDo);



        /**
         * LISTVIEW
         */
        Paint paint=new Paint();
        Random rnd=new Random();


        ArrayList<ArrayList<Object>> predmeti=new ArrayList<ArrayList<Object>>();
        int i=0;
        for(ArrayList<Object> izpit :arrayIzpisPregledaIzpitov){
            boolean contains=false;
            for(ArrayList<Object> predmet: predmeti){
                if(predmet.get(0).equals(izpit.get(0))){
                    contains=true;
                }
            }

            if(!contains){
                ArrayList<Object> object=new ArrayList<Object>();
                object.add(0,izpit.get(0));//ime predmeta
                paint.setARGB(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                object.add(1,paint.getColor());
                predmeti.add(object);
            }
        }

        for(ArrayList<Object> izpit :arrayIzpisPregledaIzpitov){
            for(ArrayList<Object> predmet: predmeti) {
                if (predmet.get(0).equals(izpit.get(0))) {
                    izpit.add(2, predmet.get(1));
                }
             }
        }
        for(ArrayList<Object> naloga :arrayIzpisPregledaNalog){
            for(ArrayList<Object> predmet: predmeti) {
                if (predmet.get(0).equals(naloga.get(0))) {
                    naloga.add(2, predmet.get(1));
                }
            }
        }


        ArrayList<ArrayList<Object>> rezultati=new ArrayList<ArrayList<Object>>();
        for(ArrayList<Object> izpit :arrayIzpisPregledaIzpitov){
            ArrayList<Object> arrayListObject=new ArrayList<Object>();
            arrayListObject.add(0,1); //tip-izpit
            arrayListObject.add(1,izpit.get(1));//String[]
            arrayListObject.add(2,izpit.get(0));//ime predmeta
            arrayListObject.add(3,izpit.get(2));//color
            rezultati.add(arrayListObject);
        }

        for(ArrayList<Object> naloga :arrayIzpisPregledaNalog){
            ArrayList<Object> arrayListObject=new ArrayList<Object>();
            arrayListObject.add(0,0); //tip-naloga
            arrayListObject.add(1,naloga.get(1));//String[]
            arrayListObject.add(2,naloga.get(0));//ime predmeta
            arrayListObject.add(3,naloga.get(2));
            rezultati.add(arrayListObject);
        }

        ListAdapter adapterNaloge=new ListAdapter(getContext(),R.layout.list_naloge_layout, rezultati);

        ListView listViewNaloge = (ListView) fragmentView.findViewById(R.id.pregledIzpitovLNalogistView);
        listViewNaloge.setAdapter(adapterNaloge);
        listViewNaloge.setClickable(true);
        listViewNaloge.setBackgroundColor(Color.argb(7, 152, 152, 152));
        listViewNaloge.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg) {

                ArrayList<Object> object = (ArrayList<Object>)arg0.getItemAtPosition(position);
                String opis=object.get(1).toString();
                Intent pregled = new Intent(v.getContext(), Pregled.class);
                pregled.putExtra("opis", opis);
                startActivity(pregled);
            }
        });

        return fragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

  /*  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    */

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }





}
