package com.taskmaster.taskmaster.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.taskmaster.taskmaster.R;
import com.taskmaster.taskmaster.dodatki.DBHelper;
import com.taskmaster.taskmaster.razredi.Level;
import com.taskmaster.taskmaster.razredi.Uporabnik;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LevelFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LevelFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LevelFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private DBHelper dbh;
    private Uporabnik uporabnik;

    private ImageView profil_slika;
    private TextView vzdevek;
    private ProgressBar napredekBar;
    private TextView napredekTextView;
    private TextView level;
    private TextView status;
    private TextView pravocasno_oddane;
    private TextView pozneje_oddane;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LevelFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LevelFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LevelFragment newInstance(String param1, String param2) {
        LevelFragment fragment = new LevelFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_level, container, false);

        dbh= new DBHelper(getActivity().getApplicationContext());
        uporabnik=dbh.getLastUporabnik();
        double trenutne_tocke=dbh.getTockeForTocke(uporabnik.getId(),"trenutne");
        double mozne_tocke=dbh.getTockeForTocke(uporabnik.getId(),"mozne");
        String levelByTocke=dbh.getLevelIDByTocke(trenutne_tocke);
        Level levelObject=dbh.getLevel(levelByTocke);

        profil_slika=(ImageView)view.findViewById(R.id.profil_slika_id);
        vzdevek=(TextView) view.findViewById(R.id.vzdevek_id);
        napredekBar=(ProgressBar) view.findViewById(R.id.napredek_id);
        napredekTextView=(TextView) view.findViewById(R.id.textView_napredek);
        level=(TextView) view.findViewById(R.id.level_id);
        status=(TextView) view.findViewById(R.id.status_id);
        pravocasno_oddane=(TextView) view.findViewById(R.id.provacasno_oddane_id);
        pozneje_oddane=(TextView)view.findViewById(R.id.pozneje_oddane_id);


        profil_slika.setImageResource(uporabnik.getIkona());
        vzdevek.setText(uporabnik.getVzdevek());

        /**
         *  Statistika podatki
         */
        int oddaneNaloge = dbh.getStNalog(true);
        int neoddaneNaloge = dbh.getStNalog(false);
        int opravljeniIzpiti = dbh.getStIzpitov(true);
        int neopravljeniIzpiti = dbh.getStIzpitov(false);

        //staticno dodano, še ni brano iz baze
        napredekBar.setMax((int)mozne_tocke);
        napredekBar.setProgress((int)trenutne_tocke);
        napredekTextView.setText(trenutne_tocke+"/"+mozne_tocke);
        level.setText("Level: "+levelObject.getStLevela());
        status.setText("Status: "+levelObject.getStatus());
        pravocasno_oddane.setText("Oddane / neoddane naloge: " + oddaneNaloge + " / " + neoddaneNaloge);
        pozneje_oddane.setText("Vnešeni / nevnešeni rezultati izpitov: " + opravljeniIzpiti + " / " + neopravljeniIzpiti);



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

 /*   @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
