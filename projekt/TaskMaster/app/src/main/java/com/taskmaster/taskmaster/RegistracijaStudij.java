package com.taskmaster.taskmaster;

import android.content.Intent;
import android.content.SyncStatusObserver;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.taskmaster.taskmaster.dodatki.DBHelper;
import com.taskmaster.taskmaster.razredi.Uporabnik;

public class RegistracijaStudij extends AppCompatActivity {

    private String fakulteta;
    private String smer;
    private String vrstaStudija;
    private String letnik;
    private String semester;
    private DBHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbh = new DBHelper(getApplicationContext());
        Uporabnik uporabnik = dbh.getLastUporabnik();

        if(uporabnik.getId() != 0){
            Intent redirect = new Intent(getBaseContext(), PregledNalog.class);
            startActivity(redirect);
        }

        setContentView(R.layout.activity_registracija_studij);

        // fakulteta spinner
        Spinner fakultetaSpinner = (Spinner) findViewById(R.id.fakultetaSpinner);
        ArrayAdapter<CharSequence> fakultetaAdapter = ArrayAdapter.createFromResource(this,
                R.array.fakultetaSpinner, android.R.layout.simple_spinner_item);
        fakultetaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fakultetaSpinner.setAdapter(fakultetaAdapter);
        fakulteta = getValuesInsideParenthesis(fakultetaSpinner.getSelectedItem().toString());

        // smer spinner
        Spinner smerSpinner = (Spinner) findViewById(R.id.smerSpinner);
        ArrayAdapter<CharSequence> smerAdapter = ArrayAdapter.createFromResource(this,
                R.array.smerSpinner, android.R.layout.simple_spinner_item);
        smerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        smerSpinner.setAdapter(smerAdapter);
        smer =  getValuesInsideParenthesis(smerSpinner.getSelectedItem().toString());

        // vrsta spinner
        Spinner vrstaSpinner = (Spinner) findViewById(R.id.vrstaProgramaSprinner);
        ArrayAdapter<CharSequence> vrstaAdapter = ArrayAdapter.createFromResource(this,
                R.array.vrstaSpinner, android.R.layout.simple_spinner_item);
        vrstaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vrstaSpinner.setAdapter(vrstaAdapter);
        vrstaStudija =  getValuesInsideParenthesis(vrstaSpinner.getSelectedItem().toString());

        // letnik spinner
        Spinner letnikSpinner = (Spinner) findViewById(R.id.letnikSpinner);
        ArrayAdapter<CharSequence> letnikAdapter = ArrayAdapter.createFromResource(this,
                R.array.letnikSpinner, android.R.layout.simple_spinner_item);
        letnikAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        letnikSpinner.setAdapter(letnikAdapter);
        letnik =  getValuesInsideParenthesis(letnikSpinner.getSelectedItem().toString());

        // semester spinner
        Spinner semesterSpinner = (Spinner) findViewById(R.id.semesterSprinner);
        final ArrayAdapter<CharSequence> semesterAdapter = ArrayAdapter.createFromResource(this,
                R.array.semesterSpinner, android.R.layout.simple_spinner_item);
        semesterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        semesterSpinner.setAdapter(semesterAdapter);
        semester =  getValuesInsideParenthesis(semesterSpinner.getSelectedItem().toString());

        //button
        Button nadaljujInfoStudij = (Button) findViewById(R.id.nadaljujInformacijeStudij);
        nadaljujInfoStudij.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent registracijaOseba = new Intent(v.getContext(), RegistracijaUporabnik.class);

                registracijaOseba.putExtra("fakulteta", fakulteta.toLowerCase());
                registracijaOseba.putExtra("smer", smer);
                registracijaOseba.putExtra("vrstaStudija", vrstaStudija);
                registracijaOseba.putExtra("letnik", letnik);
                registracijaOseba.putExtra("semester", semester);

                startActivity(registracijaOseba);
            }

        });

    }

    public String getValuesInsideParenthesis(String value){

        String temp = value.substring(value.indexOf('[')+1, value.indexOf(']'));
        //System.out.println(temp);
        return temp;
    }
}
