package com.taskmaster.taskmaster;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taskmaster.taskmaster.dodatki.DBHelper;
import com.taskmaster.taskmaster.fragments.FilterFragment;
import com.taskmaster.taskmaster.fragments.LevelFragment;
import com.taskmaster.taskmaster.fragments.PregledNalogFragment;
import com.taskmaster.taskmaster.fragments.ProfilFragment;
import com.taskmaster.taskmaster.fragments.TemaFragment;
import com.taskmaster.taskmaster.razredi.Uporabnik;


public class PregledNalog extends AppCompatActivity{


    private static final String TAG_PREGLED_NALOG = "pregled nalog";
    private static final String TAG_PREGLED_LEVEL = "pregled levela";
    private static final String TAG_FITRIRANJE_PRIKAZA = "filtriranje";
    private static final String TAG_OSEBNI_PODATKI = "osebni podatki";
    private static final String TAG_TEMA = "tema";
    private static String CURRENT_TAG = TAG_PREGLED_NALOG;
    public static int CURRENT_THEME;

    LinearLayout navBarLayout;
    TextView emailNavBar;
    TextView vzdevekNavBar;
    ImageView imageViewNavBar;
    // index to identify current nav menu item
    public static int navItemIndex = 0;
    private DBHelper dbh;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private String[] activityTitles;
    private Toolbar toolbar;
    Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(getThemeByUporabnik());
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pregled_nalog);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbh = new DBHelper(getApplicationContext());
        Uporabnik uporabnik=dbh.getLastUporabnik();



        activityTitles = getResources().getStringArray(R.array.navigacija_naslovi);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setUpNavigationView();
        View navHeader = navigationView.getHeaderView(0);
        emailNavBar = (TextView) navHeader.findViewById(R.id.emailNavBar);
        vzdevekNavBar = (TextView) navHeader.findViewById(R.id.vzdevekNavBar);
        imageViewNavBar=(ImageView)navHeader.findViewById(R.id.imageViewNavBar);

        vzdevekNavBar.setText(uporabnik.getVzdevek());
        emailNavBar.setText(uporabnik.getEmail());
        imageViewNavBar.setImageResource(uporabnik.getIkona());

        mHandler = new Handler();




        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_PREGLED_NALOG;
            loadHomeFragment();
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_PREGLED_NALOG;
                loadHomeFragment();

                return;
            }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pregled_nalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setUpNavigationView() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation view item clicks here.



                switch (item.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_pregledNalog:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_PREGLED_NALOG;
                        break;
                    case R.id.nav_pregledLevel:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_PREGLED_LEVEL;
                        break;
                    case R.id.nav_filtriranjePrikaza:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_FITRIRANJE_PRIKAZA;
                        break;
                    case R.id.nav_spremembaOsebnihPodatki:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_OSEBNI_PODATKI;
                        break;
                    case R.id.nav_spremembaTeme:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_TEMA;
                        break;

                    default:
                        CURRENT_TAG = TAG_PREGLED_NALOG;
                        navItemIndex = 0;
                }

                if (item.isChecked()) {
                    item.setChecked(false);
                } else {
                    item.setChecked(true);
                }
                item.setChecked(true);


                loadHomeFragment();


                return true;

            }

        });




        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();


    }

    private void loadHomeFragment() {

        // selecting appropriate nav menu item
      //  selectNavMenu();


        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

         //   toggleFab();
            return;
        }


        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
      //  toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {

        switch (navItemIndex) {
            case 0:

                PregledNalogFragment pregledNalogFragment = new PregledNalogFragment();
                return pregledNalogFragment;

            case 1:

                LevelFragment levelFragment = new LevelFragment();
                return levelFragment;
            case 2:

                FilterFragment filterFragment = new FilterFragment();
                return filterFragment;

            case 3:

                ProfilFragment profilFragment = new ProfilFragment();
                return profilFragment;
            case 4:

                TemaFragment temaFragment = new TemaFragment();
                return temaFragment;

            default:
                return new PregledNalogFragment();
        }
    }


    private void setToolbarTitle() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        }
    }

    private int getThemeByUporabnik() {
        // Lookup in SharedPreferences etc.


        if(CURRENT_THEME==0){
            CURRENT_THEME=R.style.AppThemePokemon;
        }
        return CURRENT_THEME;

       //return R.style.AppThemePikachu;
    }



}
